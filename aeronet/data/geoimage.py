import copy

import numpy as np
import rasterio
from rasterio.vrt import WarpedVRT
from .constants import CRS_DEFAULT


def get_identity_gsd(transform):
    # resample to original size
    gsd_x = np.sqrt(transform[0] ** 2 + transform[3] ** 2)
    gsd_y = np.sqrt(transform[1] ** 2 + transform[4] ** 2)

    return gsd_x, gsd_y


def scale_transform(transform, target_gsd):
    transform = list(transform)[:6]
    if transform[2] == 0 and transform[4] == 0:
        gsd_x = transform[1]
        gsd_y = transform[5]
    else:
        gsd_x, gsd_y = get_identity_gsd(transform)
    target_gsd_x, target_gsd_y = target_gsd
    scale_x = gsd_x / target_gsd_x
    scale_y = gsd_y / target_gsd_y
    scaled_transform = copy.deepcopy(transform)
    scaled_transform[0] /= scale_x
    scaled_transform[3] /= scale_x
    scaled_transform[1] /= scale_y
    scaled_transform[4] /= scale_y

    return scaled_transform


def reproject(source, dst_crs=CRS_DEFAULT):
    with rasterio.open(source) as src:
        reprojected = WarpedVRT(src, dst_crs=dst_crs['init'])
        transform = reprojected.transform
        image = reprojected.read().squeeze()
        crs = reprojected.crs
    return image, crs, transform

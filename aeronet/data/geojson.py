import math

import geojson

from rasterio.warp import transform_geom
from rasterio.features import is_valid_geom
from rasterio import transform, warp
from .constants import CRS_DEFAULT, CRS_LATLON

def utm_zone(lat, lon):
    """
    Calculates UTM zone for latitude and longitude
    :param lat:
    :param lon:
    :return: UTM zone in format 'EPSG:32XYZ'
    """
    zone = (math.floor((lon + 180)/6) % 60) + 1
    if lat > 0:
        return 'EPSG:326' + str(zone)
    else:
        return 'EPSG:325' + str(zone)

def img_utm_zone(crs, tr, shape):
    """
    Calculates the UTM zone for the image center
    :param crs: image crs
    :param tr: image transform
    :param size: image size [height, width]
    :return: UTM zone in format 'EPSG:32XYZ'
    """
    #find image extents
    #todo: check for image size!
    #if it is more than 600 km longitude - recommend not to transform to utm at once
    #find image center
    center_xy = transform.xy(tr, shape[0]/2, shape[1]/2)
    center_latlon = warp.transform(crs, CRS_LATLON, [center_xy[0]], [center_xy[1]])
    #calc zone
    return utm_zone(center_latlon[1][0], center_latlon[0][0])

def transform_fc(collection, src_crs, dst_crs):
    new_features = []
    for feat in collection.features:
        new_geom = transform_geom(src_crs=src_crs,
                                  dst_crs=dst_crs,
                                  geom=feat.geometry)
        if new_geom['type'] == 'Polygon' and feat.geometry['type'] == 'MultiPolygon':
            new_geom['coordinates'] = [new_geom['coordinates']]
            new_geom['type'] = 'MultiPolygon'
        new_feat = geojson.Feature(geometry=new_geom, properties=feat.properties)
        new_features.append(new_feat)
    new_collection = geojson.FeatureCollection(crs=dst_crs, features=new_features)
    return new_collection

def read_as_projected(filename, dst_crs=CRS_DEFAULT):
    '''
    reads geojson featurecollection to the specified crs
    Initial geojson file has no projection and its crs must be WGS84 (EPSG:4326)
    Initial geojson can contain any type of shapes


    :param filename: full path to geojson file
    :param t_srs: target srs
    :return: geojson-like structure in target crs
    '''
    with open(filename, 'r') as file:
        collection = geojson.load(file)

    if type(dst_crs) == str and dst_crs == 'UTM':
        dst_crs = geojson_utm_zone(collection)
    #features = collection.features
    new_collection = transform_fc(collection, CRS_LATLON, dst_crs)
    return new_collection


def write_as_latlon(filename, collection, src_crs=None):
    '''
    writes projected geojson-like collection as a stanard lat-lon
    :param src_crs: coordinate reference system in which the collection is represented
    :param filename: full path to target geojson file
    :param collection: collection to write. If it does ont have key 'crs' it is assumed to be in lat-lon
    :return: geojson-like structure in target crs
    '''
    # todo: add geojson validation ?
    #if geojson.feature.FeatureCollection != type(collection):
    #    raise ValueError('collection must be a valid geojson FeatureCollection')
    if 'crs' not in collection.keys() and src_crs is None:
        # assume that it is not projected
        with open(filename, 'w') as file:
            file.write(geojson.dumps(collection))
            return
    elif src_crs is not None:
        crs = src_crs
    else:
        crs = collection['crs']
    # todo: use transform_fc here
    new_features = []
    for feat in collection['features']:
        if is_valid_geom(feat['geometry']):
            new_geom = transform_geom(src_crs=crs,
                                      dst_crs=CRS_LATLON,
                                      geom=feat.geometry)
        else:
            #raise ValueError('Invalid geometry')
            #produce empty geometry
            #or maybe raise an exception?
            new_geom = geojson.MultiPolygon(coordinates=[[[()]]])
        #transform_geom reduces 1-contour multipolygon to polygon, but we want to preserve type
        if new_geom['type'] == 'Polygon' and feat['geometry']['type'] == 'MultiPolygon':
            new_geom['coordinates'] = [new_geom['coordinates']]
            new_geom['type'] = 'MultiPolygon'
        new_feat = geojson.Feature(geometry=new_geom, properties=feat['properties'])
        new_features.append(new_feat)
    new_collection = geojson.FeatureCollection(crs=CRS_LATLON, features=new_features)

    with open(filename, 'w') as file:
        file.write(geojson.dumps(new_collection))

def vector_bbox(collection):
    '''
    :param collection: geojson FeatureCollection
    :return: min and max longitude and latitude
    '''
    #todo: return bbox if defined
    min_lon = min_lat = 1e20
    max_lon = max_lat = -1e20
    for feat in collection['features']:
        if feat['geometry']['type'] == 'Polygon':
            # coordinates[0] is outer ring, so inner ring does not affect bbox
            for c in feat['geometry']['coordinates'][0]:
                if float(c[0]) > max_lon:
                    max_lon = c[0]
                if float(c[0]) < min_lon:
                    min_lon = c[0]
                if float(c[1]) > max_lat:
                     max_lat = c[1]
                if float(c[1]) < min_lat:
                     min_lat = c[1]
        elif feat['geometry']['type'] == 'MultiPolygon':
            for poly in feat['geometry']['coordinates']:
                for c in poly[0]:
                    if float(c[0]) > max_lon:
                        max_lon = c[0]
                    if float(c[0]) < min_lon:
                        min_lon = c[0]
                    if float(c[1]) > max_lat:
                        max_lat = c[1]
                    if float(c[1]) < min_lat:
                        min_lat = c[1]
        else:
            raise NotImplementedError('Only Polygon and MultiPolygon features are supported')

    return min_lon, min_lat, max_lon, max_lat


def mul_vector_bbox(files, dst_crs=CRS_DEFAULT):
    """
    :param files: geojson file list
    :return: min and max longitude and latitude
    """
    min_x = min_y = 1e20
    max_x = max_y = -1e20
    for name in files:
        collection = read_as_projected(name, dst_crs)
        min_lon_tmp, min_lat_tmp, max_lon_tmp, max_lat_tmp = vector_bbox(collection)
        if max_lon_tmp > max_x:
            max_x = max_lon_tmp
        if min_lon_tmp < min_x:
            min_x = min_lon_tmp
        if max_lat_tmp > max_y:
            max_y = max_lat_tmp
        if min_lat_tmp < min_y:
            min_y = min_lat_tmp
    return min_x, min_y, max_x, max_y


def make_features(all_polygons, properties=None, geom_type='polygon'):
    """
    makes features list from a coordinates 4-level list, either polygon or multipolygon
    :param all_polygons:
    :param properties:
    :param geom_type:
    :return: FeatureCollection (python dictionary)
    """
    print(all_polygons[0])
    if properties is None:
        properties = {}
    elif type(properties) != dict:
        raise TypeError('Properties must be a dictionary')
    if geom_type == 'polygon':

        result = [geojson.Feature(properties=properties,
                                  geometry=geojson.Polygon(coordinates=p)) for p in all_polygons]
    elif geom_type == 'multipolygon':
        result = geojson.Feature(properties=properties,
                                 geometry=geojson.MultiPolygon(coordinates=all_polygons))
    else:
        raise ValueError('geom_type must be either polygon or multipolygon')
    return result


def make_fc(features, crs=None):
    """

    :param features:
    :param crs:
    :return:
    """
    if crs is None:
        crs = CRS_DEFAULT
    result = geojson.FeatureCollection(crs=crs, features=features)
    return result


def geojson_utm_zone(collection):
    """
    finds the appropriate zone for the center of the geojson bbox.
    Note that if the bbox longitude spread is more than 6 degrees, the zone is not optimal for all the data!
    So in case of a global map it is better eiher to split the collection by longitude, or read it according to the
    zone of a corresponding image
    :param collection: FeatureCollection (geojson-like dict), in LatLon
    :return: utm zone code in the form 'EPSG:32XYZ'
    """
    #if 'crs' in collection.keys() and collection['crs'] != CRS_LATLON:
    #    raise ValueError('Only latlon geojsons are accepted')
    bbox = vector_bbox(collection)
    center = [bbox[2] - bbox[0], bbox[3] - bbox[1]]

    return utm_zone(center[1], center[0])
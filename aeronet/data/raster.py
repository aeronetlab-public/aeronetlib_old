import rasterio
from aeronet.data.geojson import mul_vector_bbox
from affine import Affine

'''
transform is considered in affine form 
https://github.com/mapbox/rasterio/issues/86
'''


def crop_by_geo(src, dst, files=None, min_x=None, min_y=None, max_x=None, max_y=None, xshift=0, yshift=0):
    """

    :param xshift: shift of the image reading window from the actual geo position in pixels by x coord
    :param yshift: shift of the image reading window from the actual geo position in pixels by y coord
    :param src:
    :param dst:
    :param files:
    :param min_x:
    :param min_y:
    :param max_x:
    :param max_y:
    :return:
    """
    if files is None and \
            (min_x is None or min_y is None or max_x is None or max_y is None):
        return


    with rasterio.open(src, 'r') as src_file:

        profile = src_file.profile
        crs = src_file.crs
        tr = src_file.transform
        size = src_file.shape[-2:]
        # prefer bbox
        # [min_x, min_y, max_x, max_y]
        if min_x is None or min_y is None or max_x is None or max_y is None:
            min_x, min_y, max_x, max_y = mul_vector_bbox(files, crs)


            # Magic! top left corner in the image is 0,0; but projected y-coord increases to the top,
            # so the top left corner has minimum longitude and maximum latitude
        top_left = ~tr*[min_x, max_y] #pix_coord([[bbox[0]], [bbox[3]]],tr)
        bottom_right = ~tr*[max_x, min_y]#pix_coord([[bbox[2]], [bbox[1]]], tr)

        if bottom_right[0] > size[1]:
            bottom_right = (size[1], bottom_right[1])
        if bottom_right[1] > size[0]:
            bottom_right = (bottom_right[0], size[0])
        img = src_file.read(window=((int(top_left[1] + xshift), int(bottom_right[1] + xshift)),
                                     (int(top_left[0] + yshift), int(bottom_right[0] + yshift))))
        # calculate new coord of top-left corner
        new_shift = tr*top_left
        # multiply the transformations to apply shift to the corner
        tr = Affine.translation(new_shift[0]-tr[2], new_shift[1]-tr[5])*tr
    profile.update(transform=tr, width=img.shape[2], height=img.shape[1], TFW=False, compress='lzw')
    with rasterio.Env():
        # print(bottom_right)
        with rasterio.open(dst, 'w',
                           **profile) as dst_file:
            dst_file.write(img)
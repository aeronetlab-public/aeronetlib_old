

from keras.callbacks import Callback


class UnlockLayers(Callback):

    def __init__(self, step_size, start_iteration=0):
        super(UnlockLayers, self).__init__()
        self.step_size = step_size
        self.start_iteration = start_iteration
        self.ul_iterations = 0.
        self.history = {}
            
    def on_batch_end(self, epoch, logs=None):
        
        self.ul_iterations += 1
        
        if (self.ul_iterations % self.step_size == 0) and (self.ul_iterations > self.start_iteration):
            unlock_layer = None
            for layer in self.model.layers:
                if not layer.trainable:
                    unlock_layer = layer

            if unlock_layer:
                print(unlock_layer.name)
                unlock_layer.trainable = True
                self.model.compile(optimizer=self.model.optimizer, 
                                   loss=self.model.loss,
                                   metrics=self.model.metrics,
                                   loss_weights=self.model.loss_weights,
                                   sample_weight_mode=self.model.sample_weight_mode,
                                   weighted_metrics=self.model.weighted_metrics)


class UnlockLayersMultiGPU(Callback):

    def __init__(self, step_size, start_iteration=0):
        super(UnlockLayersMultiGPU, self).__init__()
        self.step_size = step_size
        self.start_iteration = start_iteration
        self.ul_iterations = 0.
        self.history = {}
            
    def on_batch_end(self, epoch, logs=None):
        
        self.ul_iterations += 1
        
        if (self.ul_iterations % self.step_size == 0) and (self.ul_iterations > self.start_iteration):
            unlock_layer = None
            for layer in self.model.layers[3].layers:
                if not layer.trainable:
                    unlock_layer = layer

            if unlock_layer:
                print(unlock_layer.name)
                unlock_layer.trainable = True
                self.model.compile(optimizer=self.model.optimizer, 
                                   loss=self.model.loss,
                                   metrics=self.model.metrics,
                                   loss_weights=self.model.loss_weights,
                                   sample_weight_mode=self.model.sample_weight_mode,
                                   weighted_metrics=self.model.weighted_metrics)

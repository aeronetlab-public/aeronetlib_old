from keras import backend as K
from keras.losses import binary_crossentropy

from aeronet.models_zoo.vgg19 import get_VGG19

smooth = 0.001

def jaccard_loss(y_true, y_pred, weights=1):
    
    intersection = K.sum(y_true * y_pred, axis=[0, 1, 2])
    sum_ = K.sum(y_true + y_pred, axis=[0, 1, 2])

    jac = 1 - (intersection + smooth) / (sum_ - intersection + smooth)
    jac = jac * weights
    return K.mean(jac)


def f1_loss(y_true, y_pred):
    # axes = np.arange(y_true.ndim)[:-1]
    axes = [0, 1, 2]
    intersection = K.sum(y_true * y_pred, axis=axes)
    f1_score = (intersection + smooth) / (K.sum(y_true, axis=axes) + K.sum(y_true, axis=axes) + smooth)
    return K.mean(1 - f1_score)


def boundary_loss(y_true, y_pred):
    
    theta = 3
    
    if K.ndim(y_pred) == 3:
        y_pred = K.expand_dims(y_pred, -1)
        y_true = K.expand_dims(y_true, -1)
    
    y_true_boundary = K.pool2d(y_true, (5, 5), padding="same") - y_true
    y_pred_boundary = K.pool2d(y_pred, (5, 5), padding="same") - y_pred

    y_true_big_boundary = K.pool2d(y_true_boundary, (theta, theta), padding="same")
    y_pred_big_boundary = K.pool2d(y_pred_boundary, (theta, theta), padding="same")
    
    boundary_l = f1_loss(y_true_big_boundary, y_pred_big_boundary)
    return boundary_l


def bce_jaccard_loss(y_true, y_pred):
    return binary_crossentropy(y_true, y_pred) + jaccard_loss(y_true, y_pred)


def bce_f1_loss(y_true, y_pred):
    return binary_crossentropy(y_true, y_pred) + f1_loss(y_true, y_pred)

   
def f1_boundary_loss(y_true, y_pred, weights=0.5):
    boundary_l = boundary_loss(y_true, y_pred)
    f1_l = f1_loss(y_true, y_pred)
    return weights * boundary_l + (1 - weights) * f1_l


def jaccard_boundary_loss(y_true, y_pred, weights=0.5):
    boundary_l = boundary_loss(y_true, y_pred)
    jaccard_l = jaccard_loss(y_true, y_pred)
    return weights * boundary_l + (1 - weights) * jaccard_l


class VGG_loss(object):
    
    def __init__(self, layers_weights=1):

        vgg = get_VGG19(input_shape=(None,None,3))
        for l in vgg.layers:
            l.trainable = False
        self.vgg = vgg
        self.layers_weights = K.cast_to_floatx(layers_weights)#*np.array([])
        
    def __call__(self, y_true, y_pred, weights=1):
        return self._evaluate(y_true, y_pred, weights=1)
    
    def _evaluate(self, y_true, y_pred, weights=1):
#         y_true = y_true*255-127
#         y_pred = y_pred*255-127
        
        y_true_cls_3D = K.concatenate([y_true,y_true,y_true], axis = -1)
        y_pred_cls_3D = K.concatenate([y_pred,y_pred,y_pred],axis = -1)
        features_true = self.vgg(y_true_cls_3D)
        features_pred = self.vgg(y_pred_cls_3D)
        losses = []
        for feature_true, feature_pred in list (zip(features_true,features_pred)):
            loss = K.mean(K.square(feature_true-feature_pred), axis = [0,1,2])*weights
            losses.append(K.mean(loss))
        losses_tensor = K.stack(losses)
        losses_tensor = losses_tensor*K.stack(self.layers_weights)
        return K.mean(losses_tensor)
        
    def loss(self, y_true, y_pred, weights=1):
        print ('VGG_loss: self.loss is deprecated, VGG_loss is now callable object.')
        return self._evaluate(y_true, y_pred, weights=1)

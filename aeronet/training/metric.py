# A set of metrics for use during the model training and validation

from keras import backend as K


def jaccard_coef(y_true, y_pred):
    # __author__ = Vladimir Iglovikov
    smooth = 0.001
    intersection = K.sum(y_true * y_pred, axis=[0, 1, 2])
    sum_ = K.sum(y_true + y_pred, axis=[0, 1, 2])

    jac = (intersection + smooth) / (sum_ - intersection + smooth)

    return K.mean(jac)


def jaccard_coef_int(y_true, y_pred):
    # __author__ = Vladimir Iglovikov
    smooth = 0.001
    y_pred_pos = K.round(K.clip(y_pred, 0, 1))

    intersection = K.sum(y_true * y_pred_pos, axis=[0, 1, 2])
    sum_ = K.sum(y_true + y_pred, axis=[0, 1, 2])
    jac = (intersection + smooth) / (sum_ - intersection + smooth)
    return K.mean(jac)


def recall(y_true, y_pred):
    """Recall metric.

    Only computes a batch-wise average of recall.

    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)), axis=[0, 1, 2])
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)), axis=[0, 1, 2])
    recall_ = true_positives / (possible_positives + K.epsilon())
    return recall_


def precision(y_true, y_pred):
    """Precision metric.

    Only computes a batch-wise average of precision.

    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)), axis=[0, 1, 2])
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)), axis=[0, 1, 2])
    precision_ = true_positives / (predicted_positives + K.epsilon())
    return precision_


def f1(y_true, y_pred):
    precision_ = precision(y_true, y_pred)
    recall_ = recall(y_true, y_pred)
    return 2 * ((precision_ * recall_) / (precision_ + recall_))

def boundary_f1(y_true, y_pred):
    smooth = 0.001
    theta = 3
    y_pred = K.reshape(y_pred, (-1, -1, -1, 1))
    y_true = K.reshape(y_true, (-1, -1, -1, 1))
    y_pred = K.round(y_pred)
    y_true_boundary = K.pool2d(y_true, (5, 5), (1, 1), padding="same", pool_mode="max", data_format='channels_last') - y_true
    y_pred_boundary = K.pool2d(y_pred, (5, 5), (1, 1), padding="same", pool_mode="max", data_format='channels_last') - y_pred

    y_true_big_boundary = K.pool2d(y_true_boundary, (theta, theta), (1, 1), padding="same", pool_mode="max", data_format='channels_last')
    y_pred_big_boundary = K.pool2d(y_pred_boundary, (theta, theta), (1, 1), padding="same", pool_mode="max", data_format='channels_last')

    precision = K.sum(y_pred_boundary * y_true_big_boundary, axis=[0, 1, 2, 3])/(K.sum(y_pred_boundary, axis=[0, 1, 2, 3]) + smooth)
    recall = K.sum(y_true_boundary * y_pred_big_boundary, axis=[0, 1, 2, 3])/(K.sum(y_true_boundary, axis=[0, 1, 2, 3]) + smooth)
    f1 = 2. * precision * recall / (precision + recall + smooth)

    return f1

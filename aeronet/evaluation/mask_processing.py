import numpy as np


def split_masks(mask, return_num=False):
    """
    From mask array to splited masks of shape [n_instances, IMG_HEIGHT, IMG_WIDTH]
    :param mask: np.array of shape [IMG_HEIGHT, IMG_WIDTH] of semantic mask
    :param return_num: bool value, True for return the n_instances value
    :return: np.array of shape [n_instances, IMG_HEIGHT, IMG_WIDTHT] of masks of instances and number of instances
    """
    from skimage.morphology import label

    masks = []
    labeled_mask, num_instances = label(mask, return_num=return_num)

    for i in np.unique(labeled_mask):
        if i != 0:
            masks.append(labeled_mask == i)

    masks = np.array(masks).astype(np.uint8)
    if return_num:
        return masks, num_instances
    else:
        return masks


# def filter_small_masks(y_pred, min_area=100):
#     """
#     Filter small objects from the binary semantic segmentation mask
#
#     :param y_pred: np.array of shape (img_shape[0], img_shape[1]), binary semantic segmentation mask
#     :param min_area: minimal acceptable area in pixels
#     :return: y_pred_filtered: filtered y_pred without objects smaller than min_area
#     """
#     from skimage import morphology as skm
#
#     labels, num = skm.label(y_pred, return_num=True)
#
#     for i in range(1, num):
#         if labels[labels == i].sum() < min_area:
#             labels[labels == i] = 0
#
#     return (labels > 0).astype(np.uint8)
#
#
def filter_small_masks(y_pred, min_area=100):
    """
    Filter small objects from the binary semantic segmentation mask

    :param y_pred: np.array of shape (img_shape[0], img_shape[1]), binary semantic segmentation mask
    :param min_area: minimal acceptable area in pixels
    :return: filtered y_pred without objects smaller than min_area
    """

    from skimage import morphology as skm
    import copy

    labels, num = skm.label(y_pred, return_num=True)
    y_pred_filtered = copy.deepcopy(y_pred)

    for i in range(1, num):
        if y_pred_filtered[labels == i].sum() < min_area:
            y_pred_filtered[labels == i] = 0

    return y_pred_filtered
from shapely.geometry import MultiPolygon
from multiprocessing import Pool
from multiprocessing import cpu_count
from tqdm import tqdm_notebook as tqdm
from copy import deepcopy


def _simplify(p):
    """Simplify shapely geometry to make it valid"""
    t = 0
    for i in range(100):
        p = p.simplify(t)
        if p.is_valid:
            return p
        t += 1
    return None


def _iou(args):
    """Function for parallel evaluation"""
    p, mp, thresh = args
    first_iou = p.intersection(mp).area / p.area
    if first_iou < thresh:
        return False

    ps = [p for p in mp]

    for pp in ps:
        iou = pp.intersection(p).area / pp.union(p).area
        if iou > thresh:
            return True
    return False


def parallel_evaluate(shapely_pr, shapely_gt, threshold=0.5, workers='max'):
    """Calculate number of true positives, false positives, false negative instances"""
    shapely_pr = deepcopy(shapely_pr)
    shapely_gt = deepcopy(shapely_gt)

    shapely_pr_s = [_simplify(p) for p in shapely_pr if _simplify(p) is not None]
    shapely_gt_s = [_simplify(p) for p in shapely_gt if _simplify(p) is not None]

    gt_mp = MultiPolygon(polygons=shapely_gt_s)
    gt_mp = _simplify(gt_mp)

    if workers == 'max':
        workers = cpu_count()
        print('Workers:', workers)

    pool = Pool(workers)

    args = [[p, gt_mp, threshold] for p in shapely_pr_s]
    res = pool.map(_iou, args)

    TP = sum(res)
    FP = len(shapely_pr) - TP
    FN = len(shapely_gt) - TP

    return TP, FP, FN


def evaluate(shapely_pr, shapely_gt, threshold=0.5):
    """Calculate number of true positives, false positives, false negative instances"""
    TP = 0

    shapely_pr = deepcopy(shapely_pr)
    shapely_gt = deepcopy(shapely_gt)

    shapely_pr_s = [_simplify(p) for p in shapely_pr if _simplify(p) is not None]
    shapely_gt_s = [_simplify(p) for p in shapely_gt if _simplify(p) is not None]

    gt_mp = MultiPolygon(polygons=shapely_gt_s)

    for i, pred_p in enumerate(tqdm(shapely_pr_s, desc='objects')):

        first_iou = pred_p.intersection(gt_mp).area / pred_p.area
        if first_iou < threshold:
            continue

        for j, gt_p in enumerate(shapely_gt_s):

            iou = pred_p.intersection(gt_p).area / pred_p.union(gt_p).area

            if iou > threshold:
                TP += 1
                shapely_gt_s.pop(j)
                break

    FP = len(shapely_pr) - TP
    FN = len(shapely_gt) - TP

    return TP, FP, FN


def f1(pr, gt, threshold=0.5, use_multiprocessing=False, workers='max'):
    """Instance F1 metric"""
    epsilon = 10e-6
    if use_multiprocessing:
        tp, fp, fn = parallel_evaluate(pr, gt, threshold=threshold, workers=workers)
    else:
        tp, fp, fn = evaluate(pr, gt, threshold=threshold)

    return 2 * (tp + epsilon) / (2 * tp + fp + fn + epsilon)

def iou(pr, gt, threshold=0.5, use_multiprocessing=False, workers='max'):
    """Instance IoU metric"""
    epsilon = 10e-6
    if use_multiprocessing:
        tp, fp, fn = parallel_evaluate(pr, gt, threshold=threshold, workers=workers)
    else:
        tp, fp, fn = evaluate(pr, gt, threshold=threshold)

    return (tp + epsilon) / (tp + fp + fn + epsilon)
import cv2
import numpy as np
from tqdm import tqdm
from .metrics import pixel_jaccard

# Objectwise metric, that is calculated with respect to the object (instance) segmentation
# A connected object in the prediciton is considered a TP if there is an object in
# the GT: IoU > threshold, otherwise it is a FP.
# All the objects in GT that have no matching objects in prediction, are FN
# metrics = F1 = TP / (TP + FN + FP)

IOU_THRESHOLD = None

def calculate_objectwise_metrics(img_pred, img_gt, threshold=0.5):
    
    global IOU_THRESHOLD
    IOU_THRESHOLD = threshold
    
    if isinstance(img_pred, str): 
        img_pred = cv2.imread(img_pred, 0)
        
    if isinstance(img_gt, str): 
        img_gt = cv2.imread(img_gt, 0)
        
    assert img_gt.shape == img_pred.shape
    contours_gt = get_contours(img_gt)
    contours_pred = get_contours(img_pred)
    return calculate_obj_classification_matrix(contours_pred, contours_gt, img_gt.shape)


def get_contours(img):

    if img.ndim != 2:
        raise ValueError('Unsapprted type of image, provide gray or rgb.')
    
    _, thresh = cv2.threshold(img, 127, 255, 0)
    _, contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    return contours


def check_counters_iou_greater_threshold(counter_1, counter_2, shape):
    black_image = np.zeros(shape, np.uint8)
    img_1_ = cv2.drawContours(black_image.copy(), [counter_1], 0, (255, 255, 255), thickness=cv2.FILLED)
    img_2_ = cv2.drawContours(black_image.copy(), [counter_2], 0, (255, 255, 255), thickness=cv2.FILLED)
    #img_1_ = cv2.cvtColor(img_1_, cv2.COLOR_BGR2GRAY)
    #img_2_ = cv2.cvtColor(img_2_, cv2.COLOR_BGR2GRAY)
    return pixel_jaccard(img_1_, img_2_) >= IOU_THRESHOLD

def calculate_obj_classification_matrix(counters_pred, counters_gt, shape):
    TP = 0
    TN = 0
    FP = 0
    checked_1 = [False] * len(counters_pred)
    checked_2 = [False] * len(counters_gt)
    for i in tqdm(range(len(counters_pred))):
        for j in range(len(counters_gt)):
            if check_counters_iou_greater_threshold(counters_pred[i], counters_gt[j], shape):
                checked_1[i] = True
                checked_2[j] = True
                TP += 1
    for i in range(len(counters_pred)):
        if not checked_1[i]:
            FP += 1
    for j in range(len(counters_gt)):
        if not checked_2[j]:
            TN += 1
    return TP, FP, TN

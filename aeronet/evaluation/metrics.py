import numpy as np

from .mask_processing import split_masks, filter_small_masks

smooth = 1e-12


def instance_average_precision(y_true, y_pred, min_iou=0.5, max_iou=0.54, verbose=True, drop_small_objects=True):
    """
    Average precision metric. For iou_min=0.5, iou_max=0.55 equal to F1-score on IOU=0.5
    :param y_true: np.array of shape [IMG_HEIGHT, IMG_WIDTH] or [n_instances, IMG_HEIGHT, IMG_WIDTH] of target mask or
        splited masks of instance labels
    :param y_pred: np.array of shape [IMG_HEIGHT, IMG_WIDTH] or [n_instances, IMG_HEIGHT, IMG_WIDTH] of predicted mask
        or splited masks of instance labels
    :param min_iou: the lower threshold for intersection over union metric
    :param max_iou: the upper threshold for intersection over union metric
    :param verbose: bool, True for print all the f1-scores for each threshold.
    :param drop_small_objects: bool: small objects from the prediction to drop.
    :return: float: average precision
    """

    if drop_small_objects and len(y_true.shape) == 2:
        # TODO: works only for flat masks (of 1 layer):
        y_pred = filter_small_masks(y_pred, min_area=100)

    if len(y_true.shape) != 3:
        y_true, num_true = split_masks(y_true, return_num=True)
    else:
        num_true = len(y_true)

    if len(y_pred.shape) != 3:
        y_pred, num_pred = split_masks(y_pred, return_num=True)
    else:
        num_pred = len(y_pred)

    if verbose:
        print("Number of true objects:", num_true)
        print("Number of predicted objects:", num_pred)

    if num_true == 0:
        return int(num_pred == 0)

    # Compute iou score for each prediction
    iou = []
    for pr in range(num_pred):
        bol = 0  # best overlap
        bun = 1e-9  # corresponding best union
        for tr in range(num_true):
            overlap = y_pred[pr] * y_true[tr]  # Intersection points
            osz = np.sum(overlap)  # Add the intersection points to see size of overlap
            if osz > bol:  # Choose the match with the biggest overlap
                bol = osz
                bun = np.sum(np.maximum(y_pred[pr], y_true[tr]))  # Union formed with sum of maxima
        iou.append(bol / bun)

    # Loop over IoU thresholds
    p = 0
    if verbose:
        print("Thresh\tTP\tFP\tFN\tPrec.")
    range_ = np.arange(min_iou, max_iou, 0.05)

    for i, t in enumerate(range_):
        matches = iou > t
        tp = np.count_nonzero(matches)  # True positives
        fp = num_pred - tp  # False positives
        fn = num_true - tp  # False negatives
        p += tp / (tp + fp + fn)
        if verbose:
            print("{:1.3f}\t{}\t{}\t{}\t{}".format(t, tp, fp, fn, np.round(p / (i + 1), decimals=5)))

    if verbose:
        print("AP\t-\t-\t-\t{}".format(np.round(p / (len(range_) + 1), decimals=5)))

    return p / float(len(range_))


def instance_f1(y_true, y_pred, iou=0.5):
    """
    F1-score for instance segmentation

    :param y_true: np.array of shape [IMG_HEIGHT, IMG_WIDTH] or [n_instances, IMG_HEIGHT, IMG_WIDTH] of target mask or
        splited masks of instance labels
    :param y_pred: np.array of shape [IMG_HEIGHT, IMG_WIDTH] or [n_instances, IMG_HEIGHT, IMG_WIDTH] of predicted mask
        or splited masks of instance labels
    :param iou: the threshold for intersection over union metric
    :return: float: f1-score value
    """
    min_iou = iou
    max_iou = iou + 0.04
    return instance_average_precision(y_true, y_pred, min_iou, max_iou, verbose=False)


def pixel_jaccard(y_true, y_pred):
    """
    __author__ = Vladimir Iglovikov
    Class-wise jaccard coefficient

    :param y_true: np.array, [n_images, img_height, img_width] or [img_height, img_width] of true masks
    :param y_pred: np.array, [n_images, img_height, img_width] or [img_height, img_width] of thresholded predictions
    :return:
    """
    intersection = np.sum(np.round(np.clip(y_true, 0, 1)) * np.round(np.clip(y_pred, 0, 1)))
    sum_ = np.sum(np.round(np.clip(y_true, 0, 1)) + np.round(np.clip(y_pred, 0, 1)))

    jac = (intersection + smooth) / (sum_ - intersection + smooth)

    return np.mean(jac)


def pixel_recall(y_true, y_pred):
    """Only computes a batch-wise average of class recall.
    Computes the recall, a metric for multi-label classification of how many relevant items are selected.

    :param y_true: np.array, [n_images, img_height, img_width] or [img_height, img_width] of true masks
    :param y_pred: np.array, [n_images, img_height, img_width] or [img_height, img_width] of thresholded predictions
    :return:
    """
    true_positives = np.sum(np.round(np.clip(y_true * y_pred, 0, 1)))
    possible_positives = np.sum(np.round(np.clip(y_true, 0, 1)))
    recall_ = true_positives / possible_positives
    return recall_


def pixel_precision(y_true, y_pred):
    """Only computes a batch-wise average of class precision.
    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.

    :param y_true: np.array, [n_images, img_height, img_width] or [img_height, img_width] of true masks
    :param y_pred: np.array, [n_images, img_height, img_width] or [img_height, img_width] of thresholded predictions
    :return: class-wise precision
    """
    true_positives = np.sum(np.round(np.clip(y_true * y_pred, 0, 1)))
    predicted_positives = np.sum(np.round(np.clip(y_pred, 0, 1)))
    precision_ = true_positives / predicted_positives
    return precision_


def pixel_f1(y_true, y_pred):
    """
    Class-wise f1-score, Only computes a batch-wise average of class f1-score for thresholded predictions

    :param y_true: np.array, [n_images, img_height, img_width] or [img_height, img_width] of true masks
    :param y_pred: np.array, [n_images, img_height, img_width] or [img_height, img_width] of thresholded predictions
    :return: f1-score
    """
    precision_ = pixel_precision(y_true, y_pred)
    recall_ = pixel_recall(y_true, y_pred)
    return 2 * ((precision_ * recall_) / (precision_ + recall_))

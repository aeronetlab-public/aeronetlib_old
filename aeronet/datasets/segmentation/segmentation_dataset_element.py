import cv2
import numpy as np
import rasterio
from rasterio.vrt import WarpedVRT
from aeronet.data.constants import CRS_DEFAULT
from rasterio.crs import CRS


class SegmentationDatasetElement:
    def __init__(self, sources, target_gsd=[0.5, -0.5]):
        self._target_gsd = target_gsd
        self._sources = sources
        self._channels = None
        self._crs = None
        self._transform = None

    def get_channels(self):
        if self._channels is None:
            print('Loading image')
            self._load_image()
        return self._channels

    def get_crs(self):
        if self._crs is None:
            self._load_image()
        return self._crs

    def get_transform(self):
        if self._transform is None:
            self._load_image()
        return self._transform

    def _load_image(self):
        if len(self._sources) == 0:
            return None
        images_data = [self._imread(img) for img in self._sources]

        channels, transform, crs = self._upsample_channels(images_data)
        self._transform = transform
        self._crs = crs
        self._channels = channels

    def _reshape_to_target_gsd(self, image, transform):
        gsd_x, gsd_y = abs(transform[0]), abs(transform[4])
        target_gsd_x, target_gsd_y = abs(self._target_gsd[0]), abs(self._target_gsd[1])

        if abs(gsd_x - target_gsd_x) / target_gsd_x > 0.1 or \
                abs(gsd_y - target_gsd_y) / target_gsd_y > 0.1:
            h, w = image.shape[:2]
            w = abs(int(w * gsd_x / target_gsd_x))
            h = abs(int(h * gsd_y / target_gsd_y))
            print(w, h, gsd_x, gsd_y)
            image = cv2.resize(image, (w, h), cv2.INTER_NEAREST)
            transform = [target_gsd_x, transform[1], transform[2], transform[3], -target_gsd_y, transform[5]]
        else:
            transform = transform[:6]
        return image, transform

    def _imread(self, image):
        '''
        Reads an image from source
        Reprojects it to internal crs
        :param image:
        :return:
        '''
        with rasterio.open(image) as src:
            if src.crs != CRS_DEFAULT:
                src = WarpedVRT(src, dst_crs= CRS(init = CRS_DEFAULT['init']))
            image = src.read().squeeze()
            crs = src.crs.to_dict()
            transform = src.transform
        return image, transform, crs

    def _upsample_channels(self, images_data):
        """
        Searches for maximum resolution in list of tuples
        Upsamples all channels to the maximum resolution
        :param images_data: list of tuples (image, transform crs)
        :return:
        """
        max_image, transform, crs = max(images_data, key=lambda image: image[0].shape[0] * image[0].shape[1])
        h, w = max_image.shape

        # resize all images to max resolution
        print('Resizing to max resolution {}px x {}px ... '.format(h, w), end='')
        upsampled_channels = [cv2.resize(image_data[0], (w, h), cv2.INTER_NEAREST) for image_data in images_data]
        print('done!')

        image = np.stack(upsampled_channels, axis=-1)
        print(*self._target_gsd)
        print('Resizing to target resolution gsd_x={}, gsd_y={} ... '.format(*self._target_gsd), end='')
        print()
        image, transform = self._reshape_to_target_gsd(image, transform)
        print('done!')

        return image, transform, crs

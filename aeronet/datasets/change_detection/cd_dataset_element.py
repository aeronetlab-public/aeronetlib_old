import datetime
import numpy as np


class ChangeDetectionDatasetElement:
    """
    Entity for series representation
    """

    def __init__(self, segmentation_dataset_elements):
        """
        dict with segmentation elements
        key - timeframe
        :param segmentation_dataset_elements:
        """
        self._elements = segmentation_dataset_elements
        self._channels = None
        self._crs = None
        self._transform = None

    def __len__(self):
        return len(self._elements)

    def get_elements(self, descending=False):
        timeframes = sorted(self._elements.keys(), reverse=descending)
        return [self._elements[timeframe] for timeframe in timeframes]

    def get_channels(self, descending=False):
        """
        flattens segmentation elements into list of channels
        ordered by timeframe keys
        :param descending:
        :return:
        """
        elements = self.get_elements(descending)
        elements_channels = np.concatenate([element.get_channels() for element in elements], axis=2)

        return elements_channels

    def get_crs(self):
        if self._crs is None:
            self._load_crs()
        return self._crs

    def get_transform(self):
        if self._transform is None:
            self._load_transform()
        return self._transform

    def _load_crs(self):
        # TODO replace with something more convenient
        self._crs = next(iter(self._elements.values())).get_crs()

    def _load_transform(self):
        # TODO replace with something more convenient
        self._transform = next(iter(self._elements.values())).get_transform()

from .iterator import Iterator
import numpy as np
import keras.backend as K
import multiprocessing.pool
import rasterio
import warnings
import skimage.transform
import os
import time
import random
import cv2
#from joblib import Parallel, delayed

class SegmentationListIterator(Iterator):
    #TODO:
    #    GSD Done
    #    Multiprocessing error (OSRead)
    #    Save to dir
    #    Maybe change interpolation in random transform (:param order:)
    #    Multiprocessing to crop
    """Iterator capable of reading patches of images from a list of paths.
    Args:
        img_paths (:obj:'list' of :obj:'list'): list of list of string paths to images. 
            The first level list corresponds for id, the second level corresponds for particular file.
        mask_paths (:obj:'list' of :obj:'list'): list of list of string paths to masks. 
            The first level list corresponds for id, the second level corresponds for particular file.
        image_data_generator (SegmentationDataGenerator): Instance of `ImageDataGenerator`
            to use for random transformations and normalization.
        num_images (int): number of images for one epoch.
        default_aug (bool): if True then default augmentation parameters are setted else augmentation is off. 
            To set each parameter of augmentation use function set_augmentation_params()
        target_size (tuple): tuple of two integers, dimensions to resize input images to.
        scale (int): scale of output image. WILL BE CHANGED TO GSD! 
        classes (list): list of strings classes id, e.g '0100' for buildings, '0400' for trees.
        channels (list): list of strings channels names, e.g 'PAN', 'SWIR1'.
        batch_size (int): size of a batch.
        shuffle (bool): whether to shuffle the data between epochs. 
        seed (int): Random seed for data shuffling.
        save_to_dir (str): Optional directory where to save the pictures
            being yielded, in a viewable format. This is useful
            for visualizing the random transformations being
            applied, for debugging purposes.
        save_prefix (str): String prefix to use for saving sample
            images (if `save_to_dir` is set).
        save_format (str): Format to use for saving sample images
            (if `save_to_dir` is set).
        target_gsd (float): ground sampling distance (GSD) of output images and masks. 
            If pictures have gifferent GSD then they are resampled to target_gsd.
            For faster preparation of batch target_gsd have to differ from pictute GSD by integer number.
        multiprocessing_kernels (:obj:'int', or None): number of kernels that will be used for batch preparation,
            if None then runs process on single kernel.
    """

    def __init__(self, img_paths, mask_paths, image_data_generator=None,
                 num_images = 10000, default_aug =False,
                 target_size=(256, 256),
                 classes=[], channels=[],
                 batch_size=32, shuffle=True, seed=None,
                 save_to_dir=None,
                 save_prefix='', save_format='tif',
                 target_gsd=None, multiprocessing_kernels=None,
                 oversampling_ratio = None, 
                 generate_background=False):
        
        if np.isscalar(target_size):
            target_size = (target_size, target_size)
        elif len(target_size) <2:
            target_size = (target_size[0], target_size[0])
        elif len(target_size) >2:
            raise ValueError('target_size cannot consist of more than 2 values!')
        target_size = tuple(target_size)
        
        if np.isscalar(target_gsd):
            target_gsd = (target_gsd, target_gsd)
        elif len(target_gsd) <2:
            target_gsd = (target_gsd[0], target_gsd[0])
        elif len(target_gsd) >2:
            raise ValueError('target_gsd cannot consist of more than 2 values!')
        target_gsd = tuple(target_gsd)
        
        # initialize parameters to save generated images
        self.save_to_dir = save_to_dir
        self.save_prefix = save_prefix
        self.save_format = save_format
        
        self.img_shape = target_size + (len(channels),)
        self.mask_shape = target_size + (len(classes),)
        self.target_size = target_size
        #self.scale = scale
        
        self.target_gsd = target_gsd
        self.channels = channels
        self.classes = classes
        self.image_data_generator = image_data_generator
        self.multiprocessing_kernels = multiprocessing_kernels
        self.oversampling_ratio = oversampling_ratio
        self.generate_background = generate_background
        
        if (len(img_paths) == 0): 
            raise Exception('There are no pictures to open!')
            
        if (len(mask_paths) == 0):
            raise Exception('There are no masks to open!')
    
        dataset_warning = ''
        for i, mask_path in enumerate(mask_paths):
            if len(mask_path)!=len(classes):
                folder_name = os.path.basename(os.path.dirname(mask_path[0]))
                dataset_warning += 'Not enough classes in '+folder_name+' folder!\n'
                del mask_paths[i]
                del img_paths[i]
            
        for i, img_path in enumerate(img_paths):
            if len(img_path)!=len(channels):
                folder_name = os.path.basename(os.path.dirname(img_path[0]))
                dataset_warning += 'Not enough channels in '+folder_name+' folder!\n'
                del mask_paths[i]
                del img_paths[i]    
        
        if (len(set(map(len,img_paths))) != 1) or (len(set(map(len,mask_paths))) != 1):
            warnings.warn(dataset_warning)
        
        self.img_files = img_paths
        self.mask_files = mask_paths
        #self.img_files = [[rasterio.open(file) for file in id_folder] for id_folder in img_paths]
        #self.mask_files = [[rasterio.open(file) for file in id_folder] for id_folder in mask_paths]
        self.samples = len(self.img_files)
        
        
        super(SegmentationListIterator, self).__init__(self.samples, num_images, batch_size, shuffle, seed, default_aug)
        #index_array = np.random.choice(np.arange(self.samples), batch_size)
        #for i in range (4):
            #batch_cache = list(map(self._form_part_batch, index_array))
    
    def _get_oversampling_indexes(self, index_array, oversampling_ratio):
        classes = self.classes
        batch_size = self.batch_size
        
        class_indexes = {}
        if not isinstance (oversampling_ratio, list):
            oversampling_ratio = [oversampling_ratio]*len(classes)
        if len(classes) != len(oversampling_ratio):
            raise ValueError('len(classes) != len(oversampling_ratio)')
        if sum(oversampling_ratio)>1:
            raise ValueError('Sum of oversampling_ratio should be less than 1')

        for i, cls in enumerate(classes):
            indexes_true = self.image_data_generator.class_matrix[cls] 
            size = np.floor(batch_size*oversampling_ratio[i]).astype('int')
            class_indexes[cls] = list(np.random.choice(indexes_true, size))

        size = batch_size - sum([len(indexes) for key, indexes in class_indexes.items()])
        notclass_indexes = list(np.random.choice(index_array, size))
        action = []
        index_array_out = []
        for key, indexes in class_indexes.items():
            index_array_out = index_array_out+indexes
            action += [key]*len(indexes)
        index_array_out += notclass_indexes
        action += [None]*len(notclass_indexes)

        combined = list(zip(action, index_array_out))
        np.random.shuffle(combined)
        action, index_array_out = (list(item) for item in zip(*combined))

        return index_array_out, action

    def _get_batches_of_transformed_samples(self, index_array):
        """Returns a batch of transformed samples.
        Args:
            index_array (:obj:nupy.ndarray or :obj:list): array of sample indices to include in batch.
            
        Returns:
            batch_x (numpy.ndarray): A batch of transformed images. 4D numpy array - [batch_size, h, w, num_channels],
                channels have the same order that is in input list for generator.  
            batch_y (numpy.ndarray): A batch of transformed images. 4D numpy array - [batch_size, h, w, num_classes],
                classes have the same order that is in input list for generator.
        """
        if self.oversampling_ratio is not None:
            index_array, actions = self._get_oversampling_indexes(index_array, self.oversampling_ratio)
        else:
            actions = [None]*len(index_array)
        #print(index_array)
        #print(actions)
        # build batch of image data
        if self.multiprocessing_kernels:
            pool = multiprocessing.pool.ThreadPool(self.multiprocessing_kernels)
           
            #batch_cache = Parallel(n_jobs=self.batch_size)(delayed(self._form_batch)(j) for j in index_array)
            batch_cache = pool.map_async(self._form_part_batch, list(zip(index_array, actions)))
            batch_cache = batch_cache.get()
            batch_x = [sample[0] for sample in batch_cache]
            batch_y = [sample[1] for sample in batch_cache]
            pool.close()
            pool.join()
        else:
            batch_cache = list(map(self._form_part_batch, list(zip(index_array, actions))))
            batch_x = [sample[0] for sample in batch_cache]
            batch_y = [sample[1] for sample in batch_cache]
        batch_x = np.array(batch_x, dtype=K.floatx())
        batch_y = np.array(batch_y, dtype=K.floatx())
        if self.generate_background:
            y_background = 1 - batch_y.sum(-1)
            y_background = np.expand_dims(y_background, -1)
            y_background = np.clip(y_background, 0, 1)
            batch_y = np.concatenate((batch_y, y_background), axis =-1)
        return batch_x, batch_y
    
    def _form_part_batch(self, data):
        """Randomly crops, augment and standartize an image and mask from rasterio orthomap object.
        Args:
            j (int): indice of orthomap array element. 
            
        Returns:
            x (nupy.ndarray): image with setted channels. 3D numpy array - [h, w, num_channels],
                channels have the same order that is in input list for generator.
            y (nupy.ndarray): mask with setted classes. 3D numpy array - [h, w, num_classes],
                classes have the same order that is in input list for generator
        """
        j = data[0]
        cls = data[1]
        
        #print(data)
        
        img_file = self.img_files[j]
        mask_file = self.mask_files[j]
        
        if cls is not None:
            coords = random.choice(self.image_data_generator.centroids_all_classes[cls][j])
            #print(len(self.image_data_generator.centroids_all_classes[cls][j]))
            #print(mask_file)
            idx = self.classes.index(cls)
            mask = rasterio.open(mask_file[idx])

            W = mask.width
            H = mask.height

            tfw_file = list(mask.transform)
            GSD_X = np.sqrt(np.square(tfw_file[0])+ np.square(tfw_file[3]))
            GSD_Y = np.sqrt(np.square(tfw_file[1])+ np.square(tfw_file[4]))

            GSD_X = np.around(GSD_X, decimals=2)
            GSD_Y = np.around(GSD_Y, decimals=2)

            W_factor = GSD_X/self.target_gsd[0]
            H_factor = GSD_Y/self.target_gsd[1]

            window_h_norm = self.target_size[0]/H/H_factor
            window_w_norm = self.target_size[1]/W/W_factor

            h = coords[0] - window_h_norm/2 + np.random.uniform(-0.3*window_h_norm, 0.3*window_h_norm)
            w = coords[1] - window_w_norm/2 +np.random.uniform(-0.3*window_w_norm, 0.3*window_w_norm)
            coords = (w,h)
            
            #print(coords)
            
        else:    
            h_normalized = np.random.uniform() 
            w_normalized = np.random.uniform()
            coords = (h_normalized, w_normalized)

        x, y = self._crop(img_file, mask_file, coords, self.target_size, self.target_gsd)
        x, y = self.random_transform(x, y, seed=self.seed)
        if self.image_data_generator:
            x = self.image_data_generator.standardize(x, self.channels)
        return x, y
    
    def _crop(self, img_file, mask_file, coords, target_size,  target_gsd):
        """Returns image and mask cropped from big orthomap.
        # Args
            image_file (list): list of paths to all desired channels within one image id.
            mask_file (list): list of paths to all desired masks within one image id.
            coords (tuple): tuple of x and y of the patch left upper corner.
            target_size (tuple): tuple of integers, dimensions of output images.
            scale (int): float scalling factor of images. e.g. 0.5 to increase SGD
        
        # Returns
            img (numpy.ndarray): channels of sample. 3D numpy array - [h, w, num_channels],
                channels have the same order that is in input list for generator.
            mask: masks of sample. 3D numpy array - [h, w, num_channels],
                channels have the same order that is in input list for generator.
        """
#         if not isinstance(target_gsd, list):
#             target_gsd = (target_gsd, target_gsd)
        
        # creating image patch
        img = np.empty(target_size+(len(img_file),),dtype = K.floatx())
        for i, img_to_read in enumerate(img_file):
            img_to_read = rasterio.open(img_to_read)
            # calculating actual GSD of given picture
            tfw_file = list(img_to_read.transform)
            GSD_X = np.sqrt(np.square(tfw_file[0])+ np.square(tfw_file[3]))
            GSD_Y = np.sqrt(np.square(tfw_file[1])+ np.square(tfw_file[4]))
            
            GSD_X = np.around(GSD_X, decimals=2)
            GSD_Y = np.around(GSD_Y, decimals=2)
            
            W = img_to_read.width
            H = img_to_read.height
            
            W_factor = GSD_X/target_gsd[0]
            H_factor = GSD_Y/target_gsd[1]
            
            # rounding W_factor and H_factor if they oo 1/they close to integer
            if W_factor>1:
                if (np.abs(W_factor-np.around(W_factor))%1 <= 0.1) :
                    W_factor = np.around(W_factor)
            if W_factor<1:
                if np.abs(1/W_factor-np.around(1/W_factor))%1 <=0.1:
                    W_factor = 1/np.around(1/W_factor)
            
            if H_factor>1:
                if (np.abs(H_factor-np.around(H_factor))%1 <= 0.1) :
                    H_factor = np.around(H_factor)
            if H_factor<1:
                if np.abs(1/H_factor-np.around(1/H_factor))%1 <=0.1:
                    H_factor = 1/np.around(1/H_factor)
            
            
            if ((1/W_factor)%1 == 0 or (W_factor)%1 == 0) and ((1/H_factor)%1 == 0 or (H_factor)%1 == 0):
                img_cache = np.empty((img_to_read.count, target_size[0], target_size[1]),dtype = img_to_read.dtypes[0])
            else:
                img_cache = np.empty((img_to_read.count, int(target_size[0]/H_factor), int(target_size[1]/W_factor)),dtype = img_to_read.dtypes[0])
                
            h_1 = int(coords[0]*H)
            w_1 = int(coords[1]*W)
            h_2 = int(h_1 + target_size[0]/H_factor)
            w_2 = int(w_1 + target_size[1]/W_factor)
            
            h_1 = np.clip(h_1, 0, H-target_size[0]/H_factor)
            w_1 = np.clip(w_1, 0, W-target_size[1]/W_factor)
            h_2 = np.clip(h_2, target_size[0]/H_factor, H)
            w_2 = np.clip(w_2, target_size[1]/W_factor, W)
            
            img_to_read.read(window = ((h_1,h_2),(w_1,w_2)), out = img_cache)
            img_cache = np.rollaxis(img_cache, 0, 3)
            if ((1/W_factor)%1 != 0 and (W_factor)%1 != 0) or ((1/H_factor)%1 != 0 and (H_factor)%1 != 0):
                #img_cache = skimage.transform.resize(img_cache, target_size, mode='reflect',preserve_range=True)
                img_cache = cv2.resize(img_cache, (target_size[1], target_size[0]), interpolation = cv2.INTER_CUBIC)
                if img_cache.ndim<3:
                    img_cache = np.expand_dims(img_cache, -1)
            img[:,:,i:i+img_cache.shape[2]] = img_cache
            
        # creating mask patch
        mask = np.empty(target_size+(len(mask_file),),dtype = K.floatx())    
        for i, mask_to_read in enumerate(mask_file):
            mask_to_read = rasterio.open(mask_to_read)
            # calculating actual GSD of given picture
            tfw_file = list(mask_to_read.transform)
            GSD_X = np.sqrt(np.square(tfw_file[0])+ np.square(tfw_file[3]))
            GSD_Y = np.sqrt(np.square(tfw_file[1])+ np.square(tfw_file[4]))
            
            GSD_X = np.around(GSD_X, decimals=2)
            GSD_Y = np.around(GSD_Y, decimals=2)
            
            W = mask_to_read.width
            H = mask_to_read.height
            
            W_factor = GSD_X/target_gsd[0]
            H_factor = GSD_Y/target_gsd[1]

            # rounding W_factor and H_factor if they oo 1/they close to integer
            if W_factor>1:
                if (np.abs(W_factor-np.around(W_factor))%1 <= 0.1) :
                    W_factor = np.around(W_factor)
            if W_factor<1:
                if np.abs(1/W_factor-np.around(1/W_factor))%1 <=0.1:
                    W_factor = 1/np.around(1/W_factor)
            
            if H_factor>1:
                if (np.abs(H_factor-np.around(H_factor))%1 <= 0.1) :
                    H_factor = np.around(H_factor)
            if H_factor<1:
                if np.abs(1/H_factor-np.around(1/H_factor))%1 <=0.1:
                    H_factor = 1/np.around(1/H_factor)
            
            if ((1/W_factor)%1 == 0 or (W_factor)%1 == 0) and ((1/H_factor)%1 == 0 or (H_factor)%1 == 0):
                mask_cache = np.empty((mask_to_read.count, target_size[0], target_size[1]),dtype = mask_to_read.dtypes[0])
            else:
                mask_cache = np.empty((mask_to_read.count, int(target_size[0]/H_factor), int(target_size[1]/W_factor)),dtype = mask_to_read.dtypes[0])
                
            h_1 = int(coords[0]*H)
            w_1 = int(coords[1]*W)
            h_2 = int(h_1 + target_size[0]/H_factor)
            w_2 = int(w_1 + target_size[1]/W_factor)
            
            
            h_1 = np.clip(h_1, 0, H-target_size[0]/H_factor)
            w_1 = np.clip(w_1, 0, W-target_size[1]/W_factor)
            h_2 = np.clip(h_2, target_size[0]/H_factor, H)
            w_2 = np.clip(w_2, target_size[1]/W_factor, W)
            
            mask_to_read.read(window = ((h_1,h_2),(w_1,w_2)), out = mask_cache)
            
            mask_cache = np.rollaxis(mask_cache, 0, 3)
            if ((1/W_factor)%1 != 0 and (W_factor)%1 != 0) or ((1/H_factor)%1 != 0 and (H_factor)%1 != 0):
                #mask_cache = skimage.transform.resize(mask_cache, target_size, mode='reflect',preserve_range=True, order=0)
                mask_cache = cv2.resize(mask_cache, (target_size[1], target_size[0]), interpolation=cv2.INTER_NEAREST)
                if mask_cache.ndim<3:
                    mask_cache = np.expand_dims(mask_cache, -1)
            mask[:,:,i:i+mask_cache.shape[2]] = mask_cache

        return img, mask

    def next(self):
        """For python 2.x.
        # Returns
            The next batch.
        """
        with self.lock:
            index_array = next(self.index_generator)
        # The transformation of images is not under thread lock
        # so it can be done in parallel
        return self._get_batches_of_transformed_samples(index_array)
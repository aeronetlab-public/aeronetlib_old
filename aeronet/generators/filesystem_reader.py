import re
import os

def get_list_of_valid_filenames(directory, pattern, extension):
    """
    Go throuth all folder in given directory and return list of file_names that match pattern and extension
    
    Arguments:
    :directory: absolute path to dataset directory (should be formated as DatasetElements)
    :type directory: (str)
    :pattern: string pattern or list of string patterns (e.g. 'class' or '0100' or ['0100', '0200'])
    :type pattern: (str|list)
    :extension: string extension or list of string extensions (e.g. 'tif' or 'tiff' or ['tiff', 'tif'])
    :type extension: (str|list)

    Return:
    list of sorted lists of absolute file_names (str), lists sorted by pattern sequence
    E.g. pattern=['2', '1'], extension = 'tif' return
    [
        ['/first/element_2.tif',
         '/first/element_1.tif',]
         
        ['/second/element_2.tif',
         '/second/element_1.tif',]
    ]
    """
    
    filename_list = []
    if isinstance(extension, list):
        extension = '|'.join(extension)
        
    if isinstance(pattern, list):
        pattern_list = pattern
    elif isinstance(pattern, str):
        pattern_list = pattern.split('|')
    else:
        raise ValueError('Pattern should be string or list of strings, but get {}'.format(type(pattern)))
    
    for folder in sorted(os.listdir(directory)):
        folder_files = []
        for pt in pattern_list:
            
            # compile regular expression
            regexp = r'^{f}.*(_{pt}).*({ext})$'.format(f=folder, pt=pt, ext=extension)
            reg = re.compile(regexp)
            
            #extend to abs path
            abs_folder = os.path.join(directory, folder)
            for file in os.listdir(abs_folder):
                if reg.match(file):
                    file = os.path.join(abs_folder, file)
                    folder_files.append(file)

        if folder_files:
            filename_list.append(folder_files)
    
    return filename_list


    

def get_list_of(directory, pattern, extension):
    """
    Go throuth all folder in given directory and return list of channels or classes with special extension
    
    Arguments:
    :directory: absolute path to dataset directory (should be formated as DatasetElements)
    :type directory: (str)
    :pattern: string pattern (availiable options 'class', 'channel')
    :type pattern: (str)
    :extension: string extension or list of string extensions (e.g. 'tif' or 'tiff' or ['tiff', 'tif'])
    :type extension: (str|list)

    Return:
    list of classes or channels (e.g. pattern='class' return ['0100', '0301', '0400'])
    """
    files_list = get_list_of_valid_filenames(directory, '', extension)
    
    if isinstance(extension, list):
        extension = '|'.join(extension)
    regexp = '.*{pr}_?(.+)[_|\.].*({ext})$'.format(pr=pattern, ext=extension)
    reg = re.compile(regexp)

    target_set = set()
    for folder in files_list:
        for file_name in folder:
            if reg.match(file_name):
                target_set.add(reg.match(file_name)[1])
    return sorted(list(target_set))

def list_pictures(directory, ext='jpg|jpeg|bmp|png|ppm'):
    return [os.path.join(root, f)
            for root, _, files in os.walk(directory) for f in files
            if re.match(r'([\w]+\.(?:' + ext + '))', f)]

import numpy as np
import threading
from keras.utils.data_utils import Sequence
import keras.backend as K
from .filesystem_reader import random_channel_shift, transform_matrix_offset_center, apply_transform, flip_axis

class Iterator(Sequence):
    """Base class for image data iterators.
    Every `Iterator` must implement the `_get_batches_of_transformed_samples`
    method.
    # Arguments
        n (int): total number of samples in the dataset.
        num_images (int): number of images for one epoch.
        batch_size (tuple): size of a batch.
        shuffle (bool): boolean, whether to shuffle the data between epochs.
        seed (int): random seeding for data shuffling.
        default_aug (bool): if True then default augmentation parameters are setted else augmentation is off. 
            To set each parameter of augmentation use function set_augmentation_params()
        fill_mode (:obj:'str', optional): points outside the boundaries of the input
            are filled according to the given mode (one of `{'constant', 'nearest', 'reflect', 'wrap'}`).
        cval: Value used for points outside the boundaries of the input if `mode='constant'`.
    """

    def __init__(self, n, num_images, batch_size, shuffle, seed, default_aug=False, fill_mode='reflect', cval=0.):
        self.n = n
        self.batch_size = batch_size
        self.seed = seed
        self.shuffle = shuffle
        self.batch_index = 0
        self.total_batches_seen = 0
        self.lock = threading.Lock()
        self.index_array = None
        self.index_generator = self._flow_index()
        self.num_images = num_images
        self.channel_axis = 3
        self.row_axis = 1
        self.col_axis = 2
        self.fill_mode = fill_mode
        self.cval = cval
        self.lock = threading.Lock()

        
        
        
        if default_aug:
            self.rotation_range = 90
            self.height_shift_range = 0
            self.width_shift_range = 0
            self.shear_range = 10
            self.zoom_range = [0.8, 1.2]
            self.channel_shift_range = 20
            self.horizontal_flip = True
            self.vertical_flip = True
        else:
            self.rotation_range = 0
            self.height_shift_range = 0
            self.width_shift_range = 0
            self.shear_range = 0
            self.zoom_range = [1, 1]
            self.channel_shift_range = 0
            self.horizontal_flip = False
            self.vertical_flip = False
            
    def _set_index_array(self):
        all_pic_array = np.arange(self.n)
        index_array = np.tile(all_pic_array, self.num_images//len(all_pic_array))
        self.index_array = np.concatenate((index_array, all_pic_array[0:self.num_images%len(all_pic_array)]))
        if self.shuffle:
            self.index_array = np.random.choice(all_pic_array, self.num_images)

    def __getitem__(self, idx):
        if idx >= len(self):
            raise ValueError('Asked to retrieve element {idx}, '
                             'but the Sequence '
                             'has length {length}'.format(idx=idx,
                                                          length=len(self)))
        if self.seed is not None:
            np.random.seed(self.seed + self.total_batches_seen)
        self.total_batches_seen += 1
        if self.index_array is None:
            self._set_index_array()
        index_array = self.index_array[self.batch_size * idx:
                                       self.batch_size * (idx + 1)]
        return self._get_batches_of_transformed_samples(index_array)

    def __len__(self):
        return (self.n + self.batch_size - 1) // self.batch_size  # round up

    def on_epoch_end(self):
        self._set_index_array()

    def reset(self):
        self.batch_index = 0

    def _flow_index(self):
        # Ensure self.batch_index is 0.
        self.reset()
        while 1:
            if self.seed is not None:
                np.random.seed(self.seed + self.total_batches_seen)
            if self.batch_index == 0:
                self._set_index_array()

            current_index = (self.batch_index * self.batch_size) % self.n
            if self.n > current_index + self.batch_size:
                self.batch_index += 1
            else:
                self.batch_index = 0
            self.total_batches_seen += 1
            yield self.index_array[current_index:
                                   current_index + self.batch_size]

    def __iter__(self):
        # Needed if we want to do something like:
        # for x, y in data_gen.flow(...):
        return self

    def __next__(self, *args, **kwargs):
        return self.next(*args, **kwargs)

    def _get_batches_of_transformed_samples(self, index_array):
        """Gets a batch of transformed samples.
        # Args:
            index_array (numpy.ndarray): array of sample indices to include in batch.
        # Returns:
            A batch of transformed samples.
        """
        raise NotImplementedError
    
    def set_augmentation_params(self, rotation_range=None, height_shift_range=None, width_shift_range=None,
                                shear_range=None, zoom_range=None, channel_shift_range=None, 
                                horizontal_flip=None, vertical_flip=None):
        
        if rotation_range is not None:
            self.rotation_range = rotation_range
        if height_shift_range is not None:
            self.height_shift_range = height_shift_range
        if width_shift_range is not None:
            self.width_shift_range = width_shift_range
        if shear_range is not None:
            self.shear_range = shear_range
        if zoom_range is not None:
            self.zoom_range = zoom_range
        if channel_shift_range is not None:
            self.channel_shift_range = channel_shift_range
        if horizontal_flip is not None:
            self.horizontal_flip = horizontal_flip
        if vertical_flip is not None:
            self.vertical_flip = vertical_flip
    
    def set_params(self,**kwargs):
        for param, value in kwargs.items():
            if param in self.__dict__.keys():
                setattr(self,param,value)
    
    def random_transform(self, x, y, seed=None):
        """Randomly augment a single image and mask tensors.
        # Args:
            x (numpy.ndarray): 3D tensor, single image.
            y (numpy.ndarray): 3D tensor, single mask.
            seed (int): random seed.
        # Returns:
            x (numpy.ndarray): randomly transformed single image.
            y (numpy.ndarray): randomly transformed single mask.
            
        """
        # x is a single image, so it doesn't have image number at index 0
        img_row_axis = self.row_axis - 1
        img_col_axis = self.col_axis - 1
        img_channel_axis = self.channel_axis - 1

        if seed is not None:
            np.random.seed(seed)

        # use composition of homographies
        # to generate final transform that needs to be applied
        if self.rotation_range:
            theta = np.deg2rad(np.random.uniform(-self.rotation_range, self.rotation_range))
        else:
            theta = 0

        if self.height_shift_range:
            tx = np.random.uniform(-self.height_shift_range, self.height_shift_range)
            if self.height_shift_range < 1:
                tx *= x.shape[img_row_axis]
        else:
            tx = 0

        if self.width_shift_range:
            ty = np.random.uniform(-self.width_shift_range, self.width_shift_range)
            if self.width_shift_range < 1:
                ty *= x.shape[img_col_axis]
        else:
            ty = 0

        if self.shear_range:
            shear = np.deg2rad(np.random.uniform(-self.shear_range, self.shear_range))
        else:
            shear = 0

            
        if np.isscalar(self.zoom_range):
            self.zoom_range = [1 - self.zoom_range, 1 + self.zoom_range]
        elif len(self.zoom_range) == 2:
            self.zoom_range = list(self.zoom_range)
        else:
            raise ValueError('`zoom_range` should be a float or '
                             'a tuple or list of two floats. '
                             'Received arg: ', self.zoom_range)    
        
        
        if self.zoom_range[0] == 1 and self.zoom_range[1] == 1:
            zx, zy = 1, 1
        else:
            zx, zy = np.random.uniform(self.zoom_range[0], self.zoom_range[1], 2)

        transform_matrix = None
        if theta != 0:
            rotation_matrix = np.array([[np.cos(theta), -np.sin(theta), 0],
                                        [np.sin(theta), np.cos(theta), 0],
                                        [0, 0, 1]])
            transform_matrix = rotation_matrix

        if tx != 0 or ty != 0:
            shift_matrix = np.array([[1, 0, tx],
                                     [0, 1, ty],
                                     [0, 0, 1]])
            transform_matrix = shift_matrix if transform_matrix is None \
                                            else np.dot(transform_matrix, shift_matrix)

        if shear != 0:
            shear_matrix = np.array([[1, -np.sin(shear), 0],
                                    [0, np.cos(shear), 0],
                                    [0, 0, 1]])
            transform_matrix = shear_matrix if transform_matrix is None \
                                            else np.dot(transform_matrix, shear_matrix)

        if zx != 1 or zy != 1:
            zoom_matrix = np.array([[zx, 0, 0],
                                    [0, zy, 0],
                                    [0, 0, 1]])
            transform_matrix = zoom_matrix if transform_matrix is None \
                                           else np.dot(transform_matrix, zoom_matrix)

        if transform_matrix is not None:
            h, w = x.shape[img_row_axis], x.shape[img_col_axis]
            transform_matrix = transform_matrix_offset_center(transform_matrix, h, w)
            x = apply_transform(x, transform_matrix, img_channel_axis,
                                fill_mode=self.fill_mode, cval=self.cval)
            y = apply_transform(y, transform_matrix, img_channel_axis,
                                fill_mode=self.fill_mode, cval=self.cval, order = 0)

        if self.channel_shift_range != 0:
            x = random_channel_shift(x,
                                     self.channel_shift_range,
                                     img_channel_axis)
        if self.horizontal_flip:
            if np.random.random() < 0.5:
                x = flip_axis(x, img_col_axis)
                y = flip_axis(y, img_col_axis)

        if self.vertical_flip:
            if np.random.random() < 0.5:
                x = flip_axis(x, img_row_axis)
                y = flip_axis(y, img_row_axis)

        return x, y

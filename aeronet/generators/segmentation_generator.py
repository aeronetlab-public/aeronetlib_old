import warnings
import rasterio
import numpy as np
import shapely
from shapely.geometry import MultiPolygon

from keras import backend as K
from .filesystem_reader import get_list_of_valid_filenames
from .filesystem_reader import get_list_of
from aeronet.data.geojson import read_as_projected
from .segmentation_list_iterator import SegmentationListIterator
from .segmentation_list_iterator_unstable import SegmentationListIteratorUnSt


class SegmentationDataGenerator(object):
    """Generate minibatches of image data with real-time data augmentation.
    # Arguments
        featurewise_center: set input mean to 0 over the dataset.
        samplewise_center: set each sample mean to 0.
        featurewise_std_normalization: divide inputs by std of the dataset.
        samplewise_std_normalization: divide each input by its std.
        zca_whitening: apply ZCA whitening.
        zca_epsilon: epsilon for ZCA whitening. Default is 1e-6.
        rotation_range: degrees (0 to 180).
        width_shift_range: fraction of total width, if < 1, or pixels if >= 1.
        height_shift_range: fraction of total height, if < 1, or pixels if >= 1.
        shear_range: shear intensity (shear angle in degrees).
        zoom_range: amount of zoom. if scalar z, zoom will be randomly picked
            in the range [1-z, 1+z]. A sequence of two can be passed instead
            to select this range.
        channel_shift_range: shift range for each channel.
        fill_mode: points outside the boundaries are filled according to the
            given mode ('constant', 'nearest', 'reflect' or 'wrap'). Default
            is 'nearest'.
            Points outside the boundaries of the input are filled according to the given mode:
                'constant': kkkkkkkk|abcd|kkkkkkkk (cval=k)
                'nearest':  aaaaaaaa|abcd|dddddddd
                'reflect':  abcddcba|abcd|dcbaabcd
                'wrap':  abcdabcd|abcd|abcdabcd
        cval: value used for points outside the boundaries when fill_mode is
            'constant'. Default is 0.
        horizontal_flip: whether to randomly flip images horizontally.
        vertical_flip: whether to randomly flip images vertically.
        rescale: rescaling factor. If None or 0, no rescaling is applied,
            otherwise we multiply the data by the value provided. This is
            applied after the `preprocessing_function` (if any provided)
            but before any other transformation.
        preprocessing_function: function that will be implied on each input.
            The function will run before any other modification on it.
            The function should take one argument:
            one image (Numpy tensor with rank 3),
            and should output a Numpy tensor with the same shape.
    """

    def __init__(self,
                 featurewise_center=False,
                 samplewise_center=False,
                 featurewise_std_normalization=False,
                 samplewise_std_normalization=False,
                 zca_whitening=False,
                 zca_epsilon=1e-6,
                 rescale=None,
                 preprocessing_function=None,
                 best_gsd=0.31):
        
        self.featurewise_center = featurewise_center
        self.samplewise_center = samplewise_center
        self.featurewise_std_normalization = featurewise_std_normalization
        self.samplewise_std_normalization = samplewise_std_normalization
        self.zca_whitening = zca_whitening
        self.zca_epsilon = zca_epsilon
        self.rescale = rescale
        self.preprocessing_function = preprocessing_function

        self.channel_axis = 3
        self.row_axis = 1
        self.col_axis = 2

        self.mean = None
        self.std = None
        self.principal_components = None
        
        self.classes = None
        self.channels = None
        self.best_gsd = best_gsd
        self.classes_stat = None
        
        self.centroids_all_classes = {}
        self.class_matrix = {}

        if zca_whitening:
            raise NotImplementedError('Not implemented yet :(')
            
        if featurewise_std_normalization:
            if not featurewise_center:
                self.featurewise_center = True
                warnings.warn('This ImageDataGenerator specifies `featurewise_std_normalization`, '
                              'which overrides setting of `featurewise_center`.')
                
        if samplewise_std_normalization:
            if not samplewise_center:
                self.samplewise_center = True
                warnings.warn('This ImageDataGenerator specifies `samplewise_std_normalization`, '
                              'which overrides setting of `samplewise_center`.')
        

    def flow_from_directory(self, directory,
                            num_images=10000,
                            default_aug=False,
                            target_size=(256, 256),
                            batch_size=32, shuffle=True, seed=None,
                            classes='all',
                            channels='all',
                            save_to_dir=None,
                            save_prefix='',
                            save_format='tif',
                            target_gsd='best',
                            multiprocessing_kernels=None,
                            oversampling_ratio=None,
                            generate_background=False):
        if classes == 'all':
            if not self.classes:
                raise ValueError('please fit_from_directory first, no any information about classes')
            classes = self.classes
            
        if channels == 'all':
            if not self.channels:
                raise ValueError('please fit_from_directory first, no any information about channels')
            channels = self.channels
        
        if target_gsd == 'best':
            if not self.best_gsd:
                raise NotImplementedError
                # raise ValueError('please fit or specify SGD, no any information about sgd')
            target_gsd = self.best_gsd
        
        if self.classes:
            for cls in classes:
                if cls not in self.classes:
                    del classes[classes.index(cls)]
        if self.channels:
            for channel in channels:
                if channel not in self.channels:
                    del channels[channels.index(channel)]
        
        mask_paths = get_list_of_valid_filenames(directory, classes, 'tif')
        img_paths = get_list_of_valid_filenames(directory, channels, 'tif')
        return SegmentationListIterator(
            img_paths, mask_paths, self,
            num_images=num_images,
            default_aug=default_aug,
            target_size=target_size,
            classes=classes,
            channels=channels,
            batch_size=batch_size, shuffle=shuffle, seed=seed,
            save_to_dir=save_to_dir,
            save_prefix=save_prefix,
            save_format=save_format,
            target_gsd=target_gsd,
            multiprocessing_kernels=multiprocessing_kernels,
            oversampling_ratio=oversampling_ratio,
            generate_background=generate_background)
    
    def flow_from_directory_un_st(self, directory,
                            num_images=10000,
                            default_aug=False,
                            target_size=(256, 256),
                            batch_size=32, shuffle=True, seed=None,
                            classes='all',
                            channels='all',
                            save_to_dir=None,
                            save_prefix='',
                            save_format='tif',
                            target_gsd='best',
                            multiprocessing_kernels=None,
                            oversampling_ratio=None):
        if classes == 'all':
            if not self.classes:
                raise ValueError('please fit_from_directory first, no any information about classes')
            classes = self.classes
            
        if channels == 'all':
            if not self.channels:
                raise ValueError('please fit_from_directory first, no any information about channels')
            channels = self.channels
        
        if target_gsd == 'best':
            if not self.best_gsd:
                raise NotImplementedError
                # raise ValueError('please fit or specify SGD, no any information about sgd')
            target_gsd = self.best_gsd
        
        if self.classes:
            for cls in classes:
                if cls not in self.classes:
                    del classes[classes.index(cls)]
        if self.channels:
            for channel in channels:
                if channel not in self.channels:
                    del channels[channels.index(channel)]
        
        mask_paths = get_list_of_valid_filenames(directory, classes, 'tif')
        img_paths = get_list_of_valid_filenames(directory, channels, 'tif')
        return SegmentationListIteratorUnSt(
            img_paths, mask_paths, self,
            num_images=num_images,
            default_aug=default_aug,
            target_size=target_size,
            classes=classes,
            channels=channels,
            batch_size=batch_size, shuffle=shuffle, seed=seed,
            save_to_dir=save_to_dir,
            save_prefix=save_prefix,
            save_format=save_format,
            target_gsd=target_gsd,
            multiprocessing_kernels=multiprocessing_kernels,
            oversampling_ratio=oversampling_ratio)
    
    def standardize(self, x, channels):
        """Apply the normalization configuration to a batch of inputs.
        # Arguments
            x: batch of inputs to be normalized.
            channels: list of used channels.
        # Returns
            The inputs, normalized.
        """
        if self.preprocessing_function:
            x = self.preprocessing_function(x)
        if self.rescale:
            x *= self.rescale
        if self.samplewise_center:
            x -= np.mean(x, keepdims=True)
        if self.samplewise_std_normalization:
            x /= (np.std(x, keepdims=True) + K.epsilon())

        if self.featurewise_center:
            if self.mean is not None:
                assert x.shape[-1] == len(channels)
                mean = [self.mean[channel] for channel in channels]
                x -= np.array(mean)
            else:
                warnings.warn('This ImageDataGenerator specifies `featurewise_center`, but it '
                              'hasn\'t been fit on any training data. Fit it first by calling '
                              '`.fit_from_directory(directory)`.')
                
        if self.featurewise_std_normalization:
            if self.std is not None:
                assert x.shape[-1] == len(channels)
                std = [self.std[channel] for channel in channels]
                x /= (np.array(std) + K.epsilon())
            else:
                warnings.warn('This ImageDataGenerator specifies `featurewise_std_normalization`, '
                              'but it hasn\'t been fit on any training data. Fit it first by '
                              'calling `.fit_from_directory(directory)`.')
                
        if self.zca_whitening:
            if self.principal_components is not None:
                flatx = np.reshape(x, (-1, np.prod(x.shape[-3:])))
                whitex = np.dot(flatx, self.principal_components)
                x = np.reshape(whitex, x.shape)
            else:
                warnings.warn('This ImageDataGenerator specifies `zca_whitening`, but it hasn\'t '
                              'been imlemented')
                
        return x
    def _get_centroids_for_mask(self, json_path, mask_path):

        mask = rasterio.open(mask_path)
        polygons = read_as_projected(json_path, mask.crs)
        polygons_list = []
       
        for feature in polygons['features']:
            if feature['geometry']['type'] == 'MultiPolygon':
                for plgn in feature['geometry']['coordinates']:
                    polygons_list.append(plgn)
            else:
                polygons_list.append(feature['geometry']['coordinates'])
         
        #if polygons.features:
        #    polygons = MultiPolygon(polygons['features'][0]['geometry']['coordinates'], context_type='geojson')
        #else:
        #    polygons = MultiPolygon(polygons.features, context_type='geojson')
        polygons = MultiPolygon(polygons_list, context_type='geojson')
        
        W = mask.width
        H = mask.height
        transform = list(mask.transform)
        transform = np.array(transform).reshape(3,3)
        tm = np.linalg.inv(transform)
        tm = tm.ravel()
        tm = [tm[0],tm[1],tm[3],tm[4],tm[2],tm[5]]

        polygons = shapely.affinity.affine_transform(polygons, tm)
        centroids = []
        for polygon in polygons:
            y =  polygon.centroid.y/H 
            x = polygon.centroid.x/W
            centroids.append((x,y))
        return centroids

    def fit_from_directory(self, directory, extensions):
        """

        """

        x_files_list = get_list_of_valid_filenames(directory, 'channel', extensions)
        y_files_list = get_list_of_valid_filenames(directory, 'class', extensions)
        classes = get_list_of(directory, 'class', extensions)
        channels = get_list_of(directory, 'channel', extensions)

        print ('Found {} dataset elements:'.format(len(y_files_list)))
        print ('   {} classes: {}'.format(len(classes), ', '.join(classes)))
        print ('   {} channels: {}'.format(len(channels), ', '.join(channels)))

        self.classes = classes
        self.channels = channels

        # calculate dataset class statistics
        print ()
        print ('Calculating statistic of classes: [ {} ]'.format(' '.join(classes)))
        print ('Progress:                         [ ', end='')

        classes_stat = []
        for cls in classes:
            class_size = 0
            print ('===', end=' ')
            class_sum = 0
            f_names = get_list_of_valid_filenames(directory, 'class_{}'.format(cls), extensions)
            for f_name in f_names:
                # because we have list of lists
                f_name = f_name[0]
                img = rasterio.open(f_name).read()
                class_sum += np.sum(img)
                class_size += img.shape[2]*img.shape[1]
            classes_stat.append(round(class_sum/class_size*100, 1))
        print (']')
        classes_stat = dict(zip(classes, classes_stat))
        self.classes_stat = classes_stat


        # calculating mean and std
        print ()
        print ('Calculating channels mean and std: [ {} ]'.format(' '.join(channels)))
        print ('Progress:                          [ ', end='')
        mean = {}
        std = {}
    
        for channel in channels:
            ch_mean = []
            ch_std = []
            print ('===', end=' ')
            f_names = get_list_of_valid_filenames(directory, 'channel_{}'.format(channel), extensions)
            for f_name in f_names:
                # because we have list of lists
                f_name = f_name[0]
                img = rasterio.open(f_name).read() 
                #TODO read gsd and save it
                #transform_matrix = img.get_transform()
                ch_mean.append(np.mean(img))
                ch_std.append(np.std(img))

            mean[channel] = np.mean(ch_mean)
            std[channel] = np.mean(ch_std)
        print (']')
        
        self.mean = mean
        self.std = std
        
        # calculating centroids of class objects
        print ()
        print ('Calculating centroids of classes: [ {} ]'.format(' '.join(classes)))
        print ('Progress:                         [ ', end='')
        centroids_all_classes = {}
        class_matrix = {}
        for cls in self.classes:
            list_of_geojsons = get_list_of_valid_filenames(directory, cls, '.geojson')
            list_of_masks = get_list_of_valid_filenames(directory, cls, '.tif')
            #print(list_of_masks)
            cls_centroids = []
            class_row = []
            print ('===', end=' ')
            #print('Calculating centroids for '+cls+' class')
            for i in range (len(list_of_geojsons)):
                for j in range (len(list_of_geojsons[i])):
                    img_centroids = self._get_centroids_for_mask(list_of_geojsons[i][j],list_of_masks[i][j])
                    cls_centroids.append(img_centroids)
                    if len(img_centroids) == 0:
                        class_row.append(0)
                    else:
                        class_row.append(1)
            class_matrix[cls] = [i for i, x in enumerate(class_row) if x == 1]       
            centroids_all_classes[cls] = cls_centroids
        print (']')
        print('All centroids have been calculated!')
        
        self.centroids_all_classes = centroids_all_classes
        self.class_matrix = class_matrix
        
        
        # printing staistics
        print ('\nDataset classes balance statistics:')
        for k, v in self.classes_stat.items():
            print ('  class \'{k}\': {v}%'.format(k=k, v=v))

        print ('\nDataset channels mean and std statistics:')
        for k in self.channels:
            print ('  channel {}: mean {:.2f}; std {:.2f}'.format(k, self.mean[k], self.std[k]))
            
        
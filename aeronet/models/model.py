import os
import json
import keras
from keras.utils import multi_gpu_model
from tensorflow.python.client import device_lib

from aeronet.models.config import OrthoSegmModelConfig
from aeronet.models import SegmentationModel
from aeronet.models import E2EModel, PostsegModel


def _find_weights(weights_dir, mode='last'):
    """Find weights path if not provided manually during model initialization"""

    if mode == 'last':
        file_name = sorted(os.listdir(weights_dir))[-1]
        weights_path = os.path.join(weights_dir, file_name)

    elif mode == 'best':
        raise NotImplementedError
    else:
        raise NotImplementedError

    return weights_path


def _find_model(model_chkp_dir, mode='last'):
    """Find weights path if not provided manually during model initialization"""

    if mode == 'last':
        print(model_chkp_dir, os.listdir(model_chkp_dir))
        file_name = sorted(os.listdir(model_chkp_dir))[-1]
        model_path = os.path.join(model_chkp_dir, file_name)

    elif mode == 'best':
        raise NotImplementedError

    return model_path


def load_segmentation_model(model_dir, mode='inference', config_path='auto', graph_path='auto',
                            weights_path='auto', model_path='auto', custom_objects=None,
                            gpu_num=1):
    if config_path == 'auto':
        config_path = os.path.join(model_dir, 'config.json')

    if graph_path == 'auto':
        graph_path = os.path.join(model_dir, 'graph.json')

    if weights_path == 'auto':
        weights_dir = os.path.join(model_dir, 'weights')
        weights_path = _find_weights(weights_dir)

    if model_path == 'auto':
        model_chkp_dir = os.path.join(model_dir, 'models')
        model_path = _find_model(model_chkp_dir)

    # load configuration file
    config = OrthoSegmModelConfig.load_config(config_path)

    # load model graph file
    with open(graph_path, 'r') as f:
        graph = json.load(f)

    if mode == 'train':
        model = keras.models.load_model(model_path, custom_objects=custom_objects, compile=True)
    elif mode == 'inference':
        try:
            model = keras.models.load_model(model_path, custom_objects=custom_objects, compile=False)
        except:
            model = keras.models.model_from_json(json.dumps(graph))
            model.load_weights(weights_path)
    else:
        raise ValueError('mode must be train or inference')

    available_gpus = sum([1 for dev in device_lib.list_local_devices() if dev.device_type == 'GPU'])
    gpu_num = min(gpu_num, available_gpus)

    segmentation_model = SegmentationModel(model_dir, gpu_num)
    segmentation_model.build(model, config)

    if gpu_num > 1:
        segmentation_model.model = multi_gpu_model(segmentation_model.model, gpu_num)
    return segmentation_model


def load_cd_model(model_dir, mode='inference', config_path='auto', graph_path='auto',
                  weights_path='auto', model_path='auto', custom_objects=None,
                  gpu_num=1, model_type='end-to-end'):
    print(model_type)
    seg_model = load_segmentation_model(model_dir, mode, config_path, graph_path,
                                        weights_path, model_path, custom_objects,
                                        gpu_num)
    if model_type == 'end-to-end':
        cd_model = E2EModel(seg_model, target_gsd=seg_model.config.GSD,
                            num_channels=len(seg_model.config.CHANNELS),
                            gpu_num=gpu_num)
    elif model_type == 'post-seg':
        cd_model = PostsegModel(seg_model, target_gsd=seg_model.config.GSD,
                                num_channels=len(seg_model.config.CHANNELS),
                                gpu_num=gpu_num)
        if '101' in seg_model.config.CLASSES:
            seg_model.config.CLASSES.insert(seg_model.config.CLASSES.index('101'), '801')
            seg_model.config.CLASSES.remove('101')
    else:
        raise ValueError('Invalid model type: expected end-to-end or post-seg')

    return cd_model



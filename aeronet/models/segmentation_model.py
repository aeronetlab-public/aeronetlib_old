"""

Model is a wrapper over a set of KERAS models!
implements interface for learning over a generator + dataset or statically generated data
and for prediction (for all classes)

"""

import os
import json
import numpy as np
from keras.utils import generic_utils
from keras.models import Model
from functools import wraps

from .image_processing import get_shape
from .image_processing import pad_shape, unpad
from .image_processing import overlap_split
from .image_processing import overlap_concatenate

from aeronet.data.vector import vectorize
from aeronet.data.geoimage import scale_transform
from rasterio import Affine

from ..preprocessing import Standardizer


def _create_dir(*args):
    path = os.path.join(*args)
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def v_print(verbose):
    def f(*args, **kwargs):
        if verbose:
            print(*args, **kwargs)

    return f


class SegmentationModel(Model):
    """

    """

    def __init__(self, model_dir, gpu_num=1):

        self.config = None
        self.model = None
        self._built = False

        self.model_dir = _create_dir(model_dir)
        self.log_dir = _create_dir(model_dir, 'log')
        self.weights_dir = _create_dir(model_dir, 'weights')
        self.models_dir = _create_dir(model_dir, 'models')  # be careful with model and models! dirs
        self.gpu_num = gpu_num
        # input standardization pipeline function
        self._input_standard = None

    def __getattr__(self, attr):
        return getattr(self.model, attr)

    def build(self, model, config):
        self.model = model
        self.config = config

        # save configurations of model
        config_path = os.path.join(self.model_dir, 'config.json')
        if not os.path.exists(config_path):
            self.config.save(config_path, indent=2)

        # save graph of model
        graph_path = os.path.join(self.model_dir, 'graph.json')
        model_graph = json.loads(model.to_json())
        with open(graph_path, 'w') as f:
            json.dump(model_graph, f, indent=2)

        st = Standardizer(**self.config.STANDARDISING_PARAMS)
        self._input_standard = st.build_pipline(self.config.STANDARDISING_FUNCTIONS)

        self._built = True

    def built(func):
        @wraps(func)
        def wrapped(self, *args, **kwargs):
            if self._built:
                return func(self, *args, **kwargs)
            else:
                raise RuntimeError('Your model is not built! Please provide keras model and config.')

        return wrapped

    def tta_prediction(self, batch, n=1):
        # TTA not tested for multi_gpu!

        masks = []

        if n > 0:
            mask = self.model.predict_on_batch(batch)
            masks.append(mask)

        if n > 1:
            mask = self.model.predict_on_batch(batch[:, ::-1])[:, ::-1]
            masks.append(mask)

        if n > 2:
            mask = self.model.predict_on_batch(batch[:, :, ::-1])[:, :, ::-1]
            masks.append(mask)

        if n > 3:
            mask = self.model.predict_on_batch(batch[:, ::-1, ::-1])[:, ::-1, ::-1]
            masks.append(mask)

        masks = np.stack(masks, -1)
        mask = np.mean(masks, -1)
        return mask

    @built
    def _standardize(self, image):
        return self._input_standard(image)

    @built
    def predict_element(self, segmentation_dataset_element, split_size=1024,
                        overlap=256, verbose=0, TTA=0, batch_size=1, min_area=100, threshold=0.5):
        # TTA not tested for multi_gpu!

        print = v_print(verbose)

        print('Reading data...', end='')
        channels = segmentation_dataset_element.get_channels()

        prediction_raster = self.predict_image(channels, split_size, overlap, verbose, TTA, batch_size)
        if prediction_raster.ndim != 3:
            prediction_raster = np.expand_dims(prediction_raster, -1)
        transform = segmentation_dataset_element.get_transform()
        prediction_vector = [vectorize((prediction_raster[:, :, i] > threshold).astype(np.uint8),
                                       epsilon=0.1, min_area=min_area,
                                       transform_matrix=Affine(*transform)) for i in range(len(self.config.CLASSES))]

        return prediction_raster, prediction_vector

    @built
    def predict_image(self, channels, split_size=1024, overlap=64, verbose=0, TTA=0, batch_size=1):
        # TTA not tested for multi_gpu!
        print = v_print(verbose)
        print('segmentation inference')

        if (batch_size < self.gpu_num):
            raise ValueError('batch size must be greater than number of gpus used')
        print = v_print(verbose)
        h, w = channels.shape[:2]

        h = get_shape(h, split_size, overlap)
        w = get_shape(w, split_size, overlap)

        print('splitting...', end='')
        image, paddings = pad_shape(channels, (h, w))
        images, n_rows, n_cols = overlap_split(image, split_size, overlap)

        predictions = []
        progbar = generic_utils.Progbar(len(images), verbose=verbose)
        print('processing...')
        print('batch size: ', batch_size)
        if batch_size > 1:
            images = [self._standardize(image) for image in images]
            i = 0
            while i < len(images):  # image in images:

                if len(images) < i + 2 * batch_size:
                    end = len(images)
                else:
                    end = i + batch_size

                batch = np.stack(images[i:end])
                if not TTA:
                    pred = self.model.predict_on_batch(batch)
                else:
                    pred = self.tta_prediction(batch, TTA)
                predictions += list(pred)
                progbar.add(end - i)
                i = end
        else:
            for image in images:
                image = self._standardize(image)
                image = np.expand_dims(image, axis=0)  # input image have to be 4d tensor
                if not TTA:
                    pred = self.model.predict(image)
                else:
                    pred = self.tta_prediction(image, TTA)
                pred = np.squeeze(pred)
                predictions.append(pred)
                progbar.add(1)

        print('merging tiles...', end='')

        if len(predictions) > 1:
            prediction = overlap_concatenate(predictions, n_rows, n_cols, overlap)
        else:
            prediction = np.squeeze(predictions)
        prediction = unpad(prediction, paddings)
        print('done!')
        return prediction

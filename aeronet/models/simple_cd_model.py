"""
Simple model that makes change detection from a pair om images saved as a single dataset-element
RED, GRN, BLU - first image
PRED, PGRN, PBLU - second image

"""
from tqdm import tqdm
from aeronet.data.geoimage import scale_transform
import numpy as np
from rasterio import Affine
from rasterio.features import geometry_mask
from shapely.geometry import Polygon

import rtree
import multiprocessing
import shapely
import geojson

from aeronet.data.vector import vectorize
from .config import CdModelConfig

polygons_rtree = rtree.index.Index()





def check_near(poly_left, poly_right, max_distance=50):
    # if len(poly_left) < 3 or len(poly_right) < 3:
    #   print(poly_left, poly_right)
    #    return False
    distance = np.sqrt(((np.array(Polygon(poly_left[0]).centroid.coords.xy) -
                         np.array(Polygon(poly_right[0]).centroid.coords.xy)) ** 2).sum())

    return distance < max_distance


def check_connected(poly_left, poly_right):
    connected = Polygon(poly_left[0]).intersects(Polygon(poly_right[0]))
    return connected


def _find_change(poly):
    global polygons_rtree
    
    is_diff = True
    for instance in polygons_rtree.intersection(poly.bounds, True):
        intersection = poly.intersection(instance.object).area
        union = poly.union(instance.object).area
        if intersection / union > 0.1:
            is_diff = False
    
    if is_diff:
        return poly
    else:
        return None


def _to_multipolygons(polygons):
    polygons = shapely.geometry.MultiPolygon(polygons, context_type='geojson').buffer(-1).buffer(1).simplify(
        0.5)
    return list(polygons)

def _diff_mask(polygons_left, polygons_right, multiprocessing_kernels=0):
    """
    function for changes detection from two vector layers
    Args:
        polygons_left (:obj:shapely): list of shapely polygons or shapely multipolygon PRE-event polygons of a class
        polygons_right (:obj:shapely): list of shapely polygons or shapely multipolygon POST-event polygons of a class
        max_distance (float): max distance between pre and post instances (deprecated)
        event_type (str): type of change, one of 'vanish', 'emerge', 'all'.
        multiprocessing_kernels (int): number of kernels for parallel computing

    Returns:
        polygons_changes (list): list of features in geojson format
    """

    global polygons_rtree

    if isinstance(polygons_left, list):
        polygons_left = _to_multipolygons(polygons_left)

    if isinstance(polygons_right, list):
        polygons_right = _to_multipolygons(polygons_right)

    polygons_changes = []

    polygons_rtree = rtree.index.Index()
    for i, poly in enumerate(tqdm(polygons_left)):
        polygons_rtree.insert(i, poly.bounds, poly)

    if multiprocessing_kernels:
        pool = multiprocessing.Pool(multiprocessing_kernels)

        poly_changes = pool.map(_find_change, polygons_right)
        pool.close()
        pool.join()
    else:
        poly_changes = list(map(_find_change, polygons_right))

    poly_changes = [x for x in poly_changes if x is not None]

    poly_changes = shapely.geometry.MultiPolygon(poly_changes)
    features = [geojson.Feature(geometry=shapely.geometry.mapping(poly)) for poly in poly_changes]
    polygons_changes.extend(features)

    return polygons_changes


class CDModel():
    """
    change-detection generalized model for a series consisting of 2 images
    """

    def __init__(self, seg_model, target_gsd, num_channels, gpu_num=1, config=None):
        self.seg_model = seg_model
        self.config = seg_model.config
        self.num_channels = num_channels
        self.target_gsd = target_gsd
        self.gpu_num = gpu_num

    def predict_element(self, cd_dataset_element, split_size=1024, overlap=64, verbose=1, TTA=0,
                        batch_size=1, min_area=100, threshold=0.5, descending=False, out_format='all'):
        raise NotImplemented("Use E2EModel or PostsegModel")


class E2EModel(CDModel):
    def __init__(self, seg_model, target_gsd, num_channels, gpu_num=1):
        super().__init__(seg_model, target_gsd=target_gsd, num_channels=num_channels, gpu_num=gpu_num)
        # note that here num_channels means total number of channels in all timeframes

    def predict_element(self, cd_dataset_element, split_size=1024, overlap=64, verbose=1, TTA=0,
                        batch_size=1, min_area=100, threshold=0.5, descending=False, out_format='all'):

        if batch_size is None:
            batch_size = self.gpu_num

        series_length = len(cd_dataset_element)
        channels = cd_dataset_element.get_channels(descending)
        if channels.size() != self.num_channels * series_length:
            raise Exception(("Something was wrong with data: data.shape = {}, num_channels={}").format(channels.size(),
                                                                                                       self.num_channels))
        pred_raster = self.seg_model.predict_image(channels,
                                                   split_size=split_size, overlap=overlap,
                                                   verbose=verbose, TTA=TTA, batch_size=batch_size)

        if out_format == 'raster':
            return pred_raster
        elif out_format == 'vector' or out_format == 'all':
            transform = cd_dataset_element.get_transform()
            scaled_transform = scale_transform(transform, self.seg_model.config.GSD)
            pred_raster = np.moveaxis(pred_raster, -1, 0)
            pred_vector = [vectorize((layer > threshold).astype(np.uint8),
                                     epsilon=0.1, min_area=min_area,
                                     transform_matrix=Affine(*scaled_transform)) for layer in pred_raster]
            pred_raster = np.moveaxis(pred_raster, 0, -1)
            if out_format == 'vector':
                return pred_vector
            elif out_format == 'all':
                return pred_raster, pred_vector
        else:
            raise ValueError("Expected out_format is raster, vector or all")


class PostsegModel(CDModel):
    def __init__(self, seg_model, target_gsd, num_channels, gpu_num=1):
        super().__init__(seg_model, target_gsd=target_gsd, num_channels=num_channels, gpu_num=gpu_num)

        # Here num_channels means number of channels in each timeframe

    def predict_element(self, cd_dataset_element, split_size=1024, overlap=64, verbose=1, TTA=0,
                        batch_size=None, min_area=100, threshold=0.5, descending=True, out_format='all'):
        if batch_size is None:
            batch_size = self.gpu_num

        # find 2 timeframes and order by time
        elements = cd_dataset_element.get_elements(descending)
        predictions_vector = []
        out_shape = None
        for element in elements:
            pred_raster, pred_vector = self.seg_model.predict_element(element,
                                                                      split_size=split_size,
                                                                      overlap=overlap, verbose=verbose, TTA=TTA,
                                                                      batch_size=batch_size, min_area=min_area)
            transform = element.get_transform()
            out_shape = pred_raster.shape

            predictions_vector.append(pred_vector)

        # for 2 timeframes only!
        change_vector = [_diff_mask(predictions_vector[0][i], predictions_vector[1][i]) for i in
                         range(len(predictions_vector[0]))]

        for change_layer in change_vector:
            # feats = make_features(change_layer)
            feats = change_layer
            change_mask = []
            # empty geometry cannot be rasterized! 
            for feat in feats:
                if feats:
                    change_mask.append(geometry_mask(feat['geometry']),
                                         out_shape=out_shape[:2],
                                         transform=transform,
                                         invert=True)
                else:
                    change_mask.append(np.zeros(out_shape[:2]))
        change_mask = np.moveaxis(np.stack(change_mask), 0, -1)
        change_vector = [[feat['geometry']['coordinates'] for feat in change_vector[0]]]

        if out_format == 'raster':
            return change_mask
        elif out_format == 'vector':
            return change_vector
        elif out_format == 'all':
            return change_mask, change_vector
        else:
            raise ValueError("Expected out_format is raster, vector or all")

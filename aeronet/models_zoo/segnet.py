'''
Variants of segnet
'''

from keras.models import Model
from keras.layers import Input, Conv2D, MaxPooling2D, UpSampling2D, Activation
from keras.regularizers import l2 as l2_reg
from keras.layers.normalization import BatchNormalization
from .layers import MaxPoolingWithArgmax2D, MaxUnpooling2D

def get_segnet_noskip(input_shape=(192,240,6), classes=1):
    """
    by A.Marin
    Variable input/output shape by A.Trekin
    :param input_shape: channels last; sizes divisible by 16
    :param classes: number of classes  (=output channels)
    :return: keras model
    """
    inputs = Input(input_shape)
    bn0 = BatchNormalization()(inputs)

    conv1 = Conv2D(64, (7, 7), padding='same', kernel_regularizer=l2_reg(0.001))(bn0)
    bn1 = BatchNormalization()(conv1)
    ac1 = Activation('relu')(bn1)
    mp1 = MaxPooling2D(pool_size=(2, 2), strides=2)(ac1)

    conv2 = Conv2D(128, (7, 7), padding='same', kernel_regularizer=l2_reg(0.001))(mp1)
    bn2 = BatchNormalization()(conv2)
    ac2 = Activation('relu')(bn2)
    mp2 = MaxPooling2D(pool_size=(2, 2), strides=2)(ac2)

    conv3 = Conv2D(256, (7, 7), padding='same', kernel_regularizer=l2_reg(0.001))(mp2)
    bn3 = BatchNormalization()(conv3)
    ac3 = Activation('relu')(bn3)
    mp3 = MaxPooling2D(pool_size=(2, 2), strides=2)(ac3)

    conv4 = Conv2D(512, (7, 7), padding='same', kernel_regularizer=l2_reg(0.001))(mp3)
    bn4 = BatchNormalization()(conv4)
    ac4 = Activation('relu')(bn4)
    mp4 = MaxPooling2D(pool_size=(2, 2), strides=2)(ac4)

    up5 = UpSampling2D((2, 2))(mp4)
    conv5 = Conv2D(512, (7, 7), padding='same', kernel_regularizer=l2_reg(0.001))(up5)
    bn5 = BatchNormalization()(conv5)
    ac5 = Activation('relu')(bn5)

    up6 = UpSampling2D((2, 2))(ac5)
    conv6 = Conv2D(256, (7, 7), padding='same', kernel_regularizer=l2_reg(0.001))(up6)
    bn6 = BatchNormalization()(conv6)
    ac6 = Activation('relu')(bn6)

    up7 = UpSampling2D((2, 2))(ac6)
    conv7 = Conv2D(128, (7, 7), padding='same', kernel_regularizer=l2_reg(0.001))(up7)
    bn7 = BatchNormalization()(conv7)
    ac7 = Activation('relu')(bn7)

    up8 = UpSampling2D((2, 2))(ac7)
    conv8 = Conv2D(64, (7, 7), padding='same', kernel_regularizer=l2_reg(0.001))(up8)
    bn8 = BatchNormalization()(conv8)
    ac8 = Activation('relu')(bn8)

    conv9 = Conv2D(classes, (7, 7), activation='sigmoid', padding='same', kernel_regularizer=l2_reg(0.001))(ac8)

    #rh1 = Reshape((192, 240))(conv9)
    #rh1 = Flatten()(conv9)

    model = Model(inputs=inputs, outputs=conv9)
    return model

def SegNet(input_shape, classes, kernel=3, pool_size=(2, 2), activation=None):
    if activation is None:
        if classes == 1:
            activation = 'sigmoid'
        elif classes<1:
            raise ValueError('classes cannot be less than 1')
        else:
            activation = 'softmax'
    # encoder
    inputs = Input(shape=input_shape)

    conv_1 = Conv2D(64, (kernel, kernel), padding="same")(inputs)
    conv_1 = BatchNormalization()(conv_1)
    conv_1 = Activation("relu")(conv_1)
    conv_2 = Conv2D(64, (kernel, kernel), padding="same")(conv_1)
    conv_2 = BatchNormalization()(conv_2)
    conv_2 = Activation("relu")(conv_2)

    pool_1, mask_1 = MaxPoolingWithArgmax2D(pool_size)(conv_2)

    conv_3 = Conv2D(128, (kernel, kernel), padding="same")(pool_1)
    conv_3 = BatchNormalization()(conv_3)
    conv_3 = Activation("relu")(conv_3)
    conv_4 = Conv2D(128, (kernel, kernel), padding="same")(conv_3)
    conv_4 = BatchNormalization()(conv_4)
    conv_4 = Activation("relu")(conv_4)

    pool_2, mask_2 = MaxPoolingWithArgmax2D(pool_size)(conv_4)

    conv_5 = Conv2D(256, (kernel, kernel), padding="same")(pool_2)
    conv_5 = BatchNormalization()(conv_5)
    conv_5 = Activation("relu")(conv_5)
    conv_6 = Conv2D(256, (kernel, kernel), padding="same")(conv_5)
    conv_6 = BatchNormalization()(conv_6)
    conv_6 = Activation("relu")(conv_6)
    conv_7 = Conv2D(256, (kernel, kernel), padding="same")(conv_6)
    conv_7 = BatchNormalization()(conv_7)
    conv_7 = Activation("relu")(conv_7)

    pool_3, mask_3 = MaxPoolingWithArgmax2D(pool_size)(conv_7)

    conv_8 = Conv2D(512, (kernel, kernel), padding="same")(pool_3)
    conv_8 = BatchNormalization()(conv_8)
    conv_8 = Activation("relu")(conv_8)
    conv_9 = Conv2D(512, (kernel, kernel), padding="same")(conv_8)
    conv_9 = BatchNormalization()(conv_9)
    conv_9 = Activation("relu")(conv_9)
    conv_10 = Conv2D(512, (kernel, kernel), padding="same")(conv_9)
    conv_10 = BatchNormalization()(conv_10)
    conv_10 = Activation("relu")(conv_10)

    pool_4, mask_4 = MaxPoolingWithArgmax2D(pool_size)(conv_10)

    conv_11 = Conv2D(512, (kernel, kernel), padding="same")(pool_4)
    conv_11 = BatchNormalization()(conv_11)
    conv_11 = Activation("relu")(conv_11)
    conv_12 = Conv2D(512, (kernel, kernel), padding="same")(conv_11)
    conv_12 = BatchNormalization()(conv_12)
    conv_12 = Activation("relu")(conv_12)
    conv_13 = Conv2D(512, (kernel, kernel), padding="same")(conv_12)
    conv_13 = BatchNormalization()(conv_13)
    conv_13 = Activation("relu")(conv_13)

    pool_5, mask_5 = MaxPoolingWithArgmax2D(pool_size)(conv_13)
    print("Build enceder done..")

    # decoder

    unpool_1 = MaxUnpooling2D(pool_size)([pool_5, mask_5])

    conv_14 = Conv2D(512, (kernel, kernel), padding="same")(unpool_1)
    conv_14 = BatchNormalization()(conv_14)
    conv_14 = Activation("relu")(conv_14)
    conv_15 = Conv2D(512, (kernel, kernel), padding="same")(conv_14)
    conv_15 = BatchNormalization()(conv_15)
    conv_15 = Activation("relu")(conv_15)
    conv_16 = Conv2D(512, (kernel, kernel), padding="same")(conv_15)
    conv_16 = BatchNormalization()(conv_16)
    conv_16 = Activation("relu")(conv_16)

    unpool_2 = MaxUnpooling2D(pool_size)([conv_16, mask_4])

    conv_17 = Conv2D(512, (kernel, kernel), padding="same")(unpool_2)
    conv_17 = BatchNormalization()(conv_17)
    conv_17 = Activation("relu")(conv_17)
    conv_18 = Conv2D(512, (kernel, kernel), padding="same")(conv_17)
    conv_18 = BatchNormalization()(conv_18)
    conv_18 = Activation("relu")(conv_18)
    conv_19 = Conv2D(256, (kernel, kernel), padding="same")(conv_18)
    conv_19 = BatchNormalization()(conv_19)
    conv_19 = Activation("relu")(conv_19)

    unpool_3 = MaxUnpooling2D(pool_size)([conv_19, mask_3])

    conv_20 = Conv2D(256, (kernel, kernel), padding="same")(unpool_3)
    conv_20 = BatchNormalization()(conv_20)
    conv_20 = Activation("relu")(conv_20)
    conv_21 = Conv2D(256, (kernel, kernel), padding="same")(conv_20)
    conv_21 = BatchNormalization()(conv_21)
    conv_21 = Activation("relu")(conv_21)
    conv_22 = Conv2D(128, (kernel, kernel), padding="same")(conv_21)
    conv_22 = BatchNormalization()(conv_22)
    conv_22 = Activation("relu")(conv_22)

    unpool_4 = MaxUnpooling2D(pool_size)([conv_22, mask_2])

    conv_23 = Conv2D(128, (kernel, kernel), padding="same")(unpool_4)
    conv_23 = BatchNormalization()(conv_23)
    conv_23 = Activation("relu")(conv_23)
    conv_24 = Conv2D(64, (kernel, kernel), padding="same")(conv_23)
    conv_24 = BatchNormalization()(conv_24)
    conv_24 = Activation("relu")(conv_24)

    unpool_5 = MaxUnpooling2D(pool_size)([conv_24, mask_1])

    conv_25 = Conv2D(64, (kernel, kernel), padding="same")(unpool_5)
    conv_25 = BatchNormalization()(conv_25)
    conv_25 = Activation("relu")(conv_25)

    conv_26 = Conv2D(classes, (1, 1), padding="valid")(conv_25)
    conv_26 = BatchNormalization()(conv_26)
   

    outputs = Activation(activation)(conv_26)
    print("Build decoder done..")

    segnet = Model(inputs=inputs, outputs=outputs, name="SegNet")

    return segnet
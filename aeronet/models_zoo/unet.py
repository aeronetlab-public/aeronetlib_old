import os

import keras.backend as K

from keras.models import Model
from keras.layers import Input
from keras.layers import Conv2D
from keras.layers import Cropping2D
from keras.layers import Activation
from keras.layers import concatenate
from keras.layers import MaxPooling2D
from keras.layers import UpSampling2D
from keras.utils.data_utils import get_file
from keras.layers.advanced_activations import ELU
from keras.layers.normalization import BatchNormalization

from .layers import CroppingLayer


def get_unet_VGG16_bn(input_shape, n_classes=1, weights_path=None):
    inputs = Input(shape=(None, None, input_shape[-1]))

    # Block 1

    if input_shape[-1] != 3:
        # for normal RGB pictures:
        first_block_name = 'block1_conv1_modified'
    else:
        first_block_name = 'block1_conv1'

    x_1 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform', name=first_block_name)(inputs)
    x_1 = BatchNormalization()(x_1)
    x_1 = Activation('relu')(x_1)
    x_1 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform', name='block1_conv2')(x_1)
    x_1 = BatchNormalization()(x_1)
    x_1 = Activation('relu')(x_1)
    pool_1 = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x_1)

    # Block 2
    x_2 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_uniform', name='block2_conv1')(pool_1)
    x_2 = BatchNormalization()(x_2)
    x_2 = Activation('relu')(x_2)
    x_2 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_uniform', name='block2_conv2')(x_2)
    x_2 = BatchNormalization()(x_2)
    x_2 = Activation('relu')(x_2)
    pool_2 = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x_2)

    # Block 3
    x_3 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform', name='block3_conv1')(pool_2)
    x_3 = BatchNormalization()(x_3)
    x_3 = Activation('relu')(x_3)
    x_3 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform', name='block3_conv2')(x_3)
    x_3 = BatchNormalization()(x_3)
    x_3 = Activation('relu')(x_3)
    x_3 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform', name='block3_conv3')(x_3)
    x_3 = BatchNormalization()(x_3)
    x_3 = Activation('relu')(x_3)
    pool_3 = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x_3)

    # Block 4
    x_4 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block4_conv1')(pool_3)
    x_4 = BatchNormalization()(x_4)
    x_4 = Activation('relu')(x_4)
    x_4 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block4_conv2')(x_4)
    x_4 = BatchNormalization()(x_4)
    x_4 = Activation('relu')(x_4)
    x_4 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block4_conv3')(x_4)
    x_4 = BatchNormalization()(x_4)
    x_4 = Activation('relu')(x_4)
    pool_4 = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x_4)

    # Block 5
    x_5 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block5_conv1')(pool_4)
    x_5 = BatchNormalization()(x_5)
    x_5 = Activation('relu')(x_5)
    x_5 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block5_conv2')(x_5)
    x_5 = BatchNormalization()(x_5)
    x_5 = Activation('relu')(x_5)
    x_5 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block5_conv3')(x_5)
    x_5 = BatchNormalization()(x_5)
    x_5 = Activation('relu')(x_5)
    # x_5 = MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool')(x)

    # Block 6
    up_6 = concatenate([UpSampling2D(size=(2, 2))(x_5), x_4], axis=3)
    x_6 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block6_conv1')(up_6)
    x_6 = BatchNormalization()(x_6)
    x_6 = Activation('relu')(x_6)
    x_6 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block6_conv2')(x_6)
    x_6 = BatchNormalization()(x_6)
    x_6 = Activation('relu')(x_6)
    x_6 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block6_conv3')(x_6)
    x_6 = BatchNormalization()(x_6)
    x_6 = Activation('relu')(x_6)

    # Block 7
    up_7 = concatenate([UpSampling2D(size=(2, 2))(x_6), x_3], axis=3)
    x_7 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform', name='block7_conv1')(up_7)
    x_7 = BatchNormalization()(x_7)
    x_7 = Activation('relu')(x_7)
    x_7 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform', name='block7_conv2')(x_7)
    x_7 = BatchNormalization()(x_7)
    x_7 = Activation('relu')(x_7)
    x_7 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform', name='block7_conv3')(x_7)
    x_7 = BatchNormalization()(x_7)
    x_7 = Activation('relu')(x_7)

    # Block 8
    up_8 = concatenate([UpSampling2D(size=(2, 2))(x_7), x_2], axis=3)
    x_8 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_uniform', name='block8_conv1')(up_8)
    x_8 = BatchNormalization()(x_8)
    x_8 = Activation('relu')(x_8)
    x_8 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_uniform', name='block8_conv2')(x_8)
    x_8 = BatchNormalization()(x_8)
    x_8 = Activation('relu')(x_8)

    # Block 9
    up_9 = concatenate([UpSampling2D(size=(2, 2))(x_8), x_1], axis=3)
    x_9 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform', name='block9_conv1')(up_9)
    x_9 = BatchNormalization()(x_9)
    x_9 = Activation('relu')(x_9)
    x_9 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform', name='block9_conv2')(x_9)
    x_9 = BatchNormalization()(x_9)
    x_9 = Activation('relu')(x_9)

    # Output
    x_10 = Conv2D(n_classes, (1, 1), activation='sigmoid', name='output')(x_9)

    model = Model(inputs=inputs, outputs=x_10)

    if weights_path:
        if weights_path == 'imagenet':
            WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.1' \
                                  '/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5 '
            weights_path = get_file('vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                    WEIGHTS_PATH_NO_TOP,
                                    cache_subdir='models')
        if os.path.exists(weights_path):
            model.load_weights(weights_path, by_name=True)

    return model


def get_unet_VGG16(input_shape, n_classes=1, weights_path=None):
    inputs = Input(shape=(None, None, input_shape[-1]))

    # Block 1
    if input_shape[-1] != 3:
        # for normal RGB pictures:
        first_block_name = 'block1_conv1_modified'
    else:
        first_block_name = 'block1_conv1'

    x_1 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform', name=first_block_name)(inputs)
    x_1 = Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv2')(x_1)
    pool_1 = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x_1)

    # Block 2
    x_2 = Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv1')(pool_1)
    x_2 = Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv2')(x_2)
    pool_2 = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x_2)

    # Block 3
    x_3 = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv1')(pool_2)
    x_3 = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv2')(x_3)
    x_3 = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv3')(x_3)
    pool_3 = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x_3)

    # Block 4
    x_4 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv1')(pool_3)
    x_4 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv2')(x_4)
    x_4 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv3')(x_4)
    pool_4 = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x_4)

    # Block 5
    x_5 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv1')(pool_4)
    x_5 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv2')(x_5)
    x_5 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv3')(x_5)
    # x_5 = MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool')(x)

    # Block 6
    up_6 = concatenate([UpSampling2D(size=(2, 2))(x_5), x_4], axis=3)
    x_6 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block6_conv1')(up_6)
    x_6 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block6_conv2')(x_6)
    x_6 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block6_conv3')(x_6)

    # Block 7
    up_7 = concatenate([UpSampling2D(size=(2, 2))(x_6), x_3], axis=3)
    x_7 = Conv2D(256, (3, 3), activation='relu', padding='same', name='block7_conv1')(up_7)
    x_7 = Conv2D(256, (3, 3), activation='relu', padding='same', name='block7_conv2')(x_7)
    x_7 = Conv2D(256, (3, 3), activation='relu', padding='same', name='block7_conv3')(x_7)

    # Block 8
    up_8 = concatenate([UpSampling2D(size=(2, 2))(x_7), x_2], axis=3)
    x_8 = Conv2D(128, (3, 3), activation='relu', padding='same', name='block8_conv1')(up_8)
    x_8 = Conv2D(128, (3, 3), activation='relu', padding='same', name='block8_conv2')(x_8)

    # Block 9
    up_9 = concatenate([UpSampling2D(size=(2, 2))(x_8), x_1], axis=3)
    x_9 = Conv2D(64, (3, 3), activation='relu', padding='same', name='block9_conv1')(up_9)
    x_9 = Conv2D(64, (3, 3), activation='relu', padding='same', name='block9_conv2')(x_9)

    # Output
    x_10 = Conv2D(n_classes, (1, 1), activation='sigmoid', name='output')(x_9)

    model = Model(inputs=inputs, outputs=x_10)

    if weights_path:
        if weights_path == 'imagenet':
            WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.1' \
                                  '/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5 '
            weights_path = get_file('vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                    WEIGHTS_PATH_NO_TOP,
                                    cache_subdir='models')
        if os.path.exists(weights_path):
            model.load_weights(weights_path, by_name=True)

    return model


def get_unet_modified(input_shape, n_classes=1, n_filters=32):
    """
    UNet without crop. May be exposed to some uncertaineties near the boundaries, but works good on our tasks
    input shape should be with "channels_last"
    """

    inputs = Input(shape=(None, None, input_shape[-1]))

    conv1 = Conv2D(n_filters, (3, 3), padding='same', kernel_initializer='he_uniform')(inputs)
    conv1 = BatchNormalization()(conv1)
    conv1 = ELU()(conv1)
    conv1 = Conv2D(n_filters, (3, 3), padding='same', kernel_initializer='he_uniform')(conv1)
    conv1 = BatchNormalization()(conv1)
    conv1 = ELU()(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(n_filters * 2, (3, 3), padding='same', kernel_initializer='he_uniform')(pool1)
    conv2 = BatchNormalization()(conv2)
    conv2 = ELU()(conv2)
    conv2 = Conv2D(n_filters * 2, (3, 3), padding='same', kernel_initializer='he_uniform')(conv2)
    conv2 = BatchNormalization()(conv2)
    conv2 = ELU()(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(n_filters * 4, (3, 3), padding='same', kernel_initializer='he_uniform')(pool2)
    conv3 = BatchNormalization()(conv3)
    conv3 = ELU()(conv3)
    conv3 = Conv2D(n_filters * 4, (3, 3), padding='same', kernel_initializer='he_uniform')(conv3)
    conv3 = BatchNormalization()(conv3)
    conv3 = ELU()(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

    conv4 = Conv2D(n_filters * 8, (3, 3), padding='same', kernel_initializer='he_uniform')(pool3)
    conv4 = BatchNormalization()(conv4)
    conv4 = ELU()(conv4)
    conv4 = Conv2D(n_filters * 8, (3, 3), padding='same', kernel_initializer='he_uniform')(conv4)
    conv4 = BatchNormalization()(conv4)
    conv4 = ELU()(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)

    conv5 = Conv2D(n_filters * 16, (3, 3), padding='same', kernel_initializer='he_uniform')(pool4)
    conv5 = BatchNormalization()(conv5)
    conv5 = ELU()(conv5)
    conv5 = Conv2D(n_filters * 16, (3, 3), padding='same', kernel_initializer='he_uniform')(conv5)
    conv5 = BatchNormalization()(conv5)
    conv5 = ELU()(conv5)

    up6 = concatenate([UpSampling2D(size=(2, 2))(conv5), conv4], axis=3)
    conv6 = Conv2D(n_filters * 8, (3, 3), padding='same', kernel_initializer='he_uniform')(up6)
    conv6 = BatchNormalization()(conv6)
    conv6 = ELU()(conv6)
    conv6 = Conv2D(n_filters * 8, (3, 3), padding='same', kernel_initializer='he_uniform')(conv6)
    conv6 = BatchNormalization()(conv6)
    conv6 = ELU()(conv6)

    up7 = concatenate([UpSampling2D(size=(2, 2))(conv6), conv3], axis=3)
    conv7 = Conv2D(n_filters * 4, (3, 3), padding='same', kernel_initializer='he_uniform')(up7)
    conv7 = BatchNormalization()(conv7)
    conv7 = ELU()(conv7)
    conv7 = Conv2D(n_filters * 4, (3, 3), padding='same', kernel_initializer='he_uniform')(conv7)
    conv7 = BatchNormalization()(conv7)
    conv7 = ELU()(conv7)

    up8 = concatenate([UpSampling2D(size=(2, 2))(conv7), conv2], axis=3)
    conv8 = Conv2D(n_filters * 2, (3, 3), padding='same', kernel_initializer='he_uniform')(up8)
    conv8 = BatchNormalization()(conv8)
    conv8 = ELU()(conv8)
    conv8 = Conv2D(n_filters * 2, (3, 3), padding='same', kernel_initializer='he_uniform')(conv8)
    conv8 = BatchNormalization()(conv8)
    conv8 = ELU()(conv8)

    up9 = concatenate([UpSampling2D(size=(2, 2))(conv8), conv1], axis=3)
    conv9 = Conv2D(n_filters, (3, 3), padding='same', kernel_initializer='he_uniform')(up9)
    conv9 = BatchNormalization()(conv9)
    conv9 = ELU()(conv9)
    conv9 = Conv2D(n_filters, (3, 3), padding='same', kernel_initializer='he_uniform')(conv9)
    conv9 = BatchNormalization()(conv9)
    conv9 = ELU()(conv9)
    conv10 = Conv2D(n_classes, (1, 1), activation='sigmoid')(conv9)

    # hash = random.getrandbits(32)
    model = Model(inputs=inputs, outputs=conv10)  # , name='U-net_{:06x}'.format(hash))

    return model


def get_unet_crop(input_shape=(572, 572, 4), classes=1):
    """
    Original unet with cropping at every pooling layer
    Input shape is 184 pixels larger than output
    :param input_shape:
    :param classes:
    :return:
    """
    inputs = Input(input_shape)

    conv1 = Conv2D(32, (3, 3), padding='same', kernel_initializer='he_uniform')(inputs)
    conv1 = BatchNormalization(axis=3)(conv1)
    conv1 = ELU()(conv1)
    conv1 = Conv2D(32, (3, 3), padding='same', kernel_initializer='he_uniform')(conv1)
    conv1 = BatchNormalization(axis=3)(conv1)
    conv1 = ELU()(conv1)

    conv1 = Cropping2D(cropping=2)(conv1)  # 568
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)  # 284

    conv2 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform')(pool1)
    conv2 = BatchNormalization(axis=3)(conv2)
    conv2 = ELU()(conv2)
    conv2 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform')(conv2)
    conv2 = BatchNormalization(axis=3)(conv2)
    conv2 = ELU()(conv2)

    conv2 = Cropping2D(cropping=2)(conv2)  # 280
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)  # 140

    conv3 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_uniform')(pool2)
    conv3 = BatchNormalization(axis=3)(conv3)
    conv3 = ELU()(conv3)
    conv3 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_uniform')(conv3)
    conv3 = BatchNormalization(axis=3)(conv3)
    conv3 = ELU()(conv3)

    conv3 = Cropping2D(cropping=2)(conv3)  # 136
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)  # 68

    conv4 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform')(pool3)
    conv4 = BatchNormalization(axis=3)(conv4)
    conv4 = ELU()(conv4)
    conv4 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform')(conv4)
    conv4 = BatchNormalization(axis=3)(conv4)
    conv4 = ELU()(conv4)

    conv4 = Cropping2D(cropping=2)(conv4)  # 64
    pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)  # 32

    conv5 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform')(pool4)
    conv5 = BatchNormalization(axis=3)(conv5)
    conv5 = ELU()(conv5)
    conv5 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform')(conv5)
    conv5 = BatchNormalization(axis=3)(conv5)
    conv5 = ELU()(conv5)
    conv5 = Cropping2D(cropping=2)(conv5)  # 28

    up6 = concatenate([UpSampling2D(size=(2, 2))(conv5), Cropping2D(cropping=4)(conv4)], axis=3)  # 56
    conv6 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform')(up6)
    conv6 = BatchNormalization(axis=3)(conv6)
    conv6 = ELU()(conv6)
    conv6 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform')(conv6)
    conv6 = BatchNormalization(axis=3)(conv6)
    conv6 = ELU()(conv6)
    conv6 = Cropping2D(cropping=2)(conv6)  # 52

    up7 = concatenate([UpSampling2D(size=(2, 2))(conv6), Cropping2D(cropping=16)(conv3)], axis=3)  # 104
    conv7 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_uniform')(up7)
    conv7 = BatchNormalization(axis=3)(conv7)
    conv7 = ELU()(conv7)
    conv7 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_uniform')(conv7)
    conv7 = BatchNormalization(axis=3)(conv7)
    conv7 = ELU()(conv7)
    conv7 = Cropping2D(cropping=2)(conv7)  # 100

    up8 = concatenate([UpSampling2D(size=(2, 2))(conv7), Cropping2D(cropping=40)(conv2)], axis=3)  # 200
    conv8 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform')(up8)
    conv8 = BatchNormalization(axis=3)(conv8)
    conv8 = ELU()(conv8)
    conv8 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform')(conv8)
    conv8 = BatchNormalization(axis=3)(conv8)
    conv8 = ELU()(conv8)
    conv8 = Cropping2D(cropping=2)(conv8)  # 196

    up9 = concatenate([UpSampling2D(size=(2, 2))(conv8), Cropping2D(cropping=88)(conv1)], axis=3)  # 392
    conv9 = Conv2D(32, (3, 3), padding='same', kernel_initializer='he_uniform')(up9)
    conv9 = BatchNormalization(axis=3)(conv9)
    conv9 = ELU()(conv9)
    conv9 = Conv2D(32, (3, 3), padding='same', kernel_initializer='he_uniform')(conv9)
    conv9 = BatchNormalization(axis=3)(conv9)
    conv9 = ELU()(conv9)
    conv9 = Cropping2D(cropping=2)(conv9)  # 388

    conv10 = Conv2D(classes, (1, 1), activation='sigmoid')(conv9)

    model = Model(inputs=inputs, outputs=conv10, name='U-net-orig')

    return model


def unet_vgg16_encoder(model_input, postfix="", weights_path=None):
    # Crop appropriate channels:
    if postfix == 'left':
        ip = CroppingLayer(0, 3)(model_input)
    else:
        ip = CroppingLayer(3, 6)(model_input)
    # Block 1:
    x_1 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform', name='block1_conv1')(ip)
    x_1 = BatchNormalization()(x_1)
    x_1 = Activation('relu')(x_1)
    x_1 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_uniform', name='block1_conv2')(x_1)
    x_1 = BatchNormalization()(x_1)
    x_1 = Activation('relu')(x_1)
    pool_1 = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x_1)
    # Block 2
    x_2 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_uniform', name='block2_conv1')(pool_1)
    x_2 = BatchNormalization()(x_2)
    x_2 = Activation('relu')(x_2)
    x_2 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_uniform', name='block2_conv2')(x_2)
    x_2 = BatchNormalization()(x_2)
    x_2 = Activation('relu')(x_2)
    pool_2 = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x_2)
    # Block 3
    x_3 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform', name='block3_conv1')(pool_2)
    x_3 = BatchNormalization()(x_3)
    x_3 = Activation('relu')(x_3)
    x_3 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform', name='block3_conv2')(x_3)
    x_3 = BatchNormalization()(x_3)
    x_3 = Activation('relu')(x_3)
    x_3 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_uniform', name='block3_conv3')(x_3)
    x_3 = BatchNormalization()(x_3)
    x_3 = Activation('relu')(x_3)
    pool_3 = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x_3)
    # Block 4
    x_4 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block4_conv1')(pool_3)
    x_4 = BatchNormalization()(x_4)
    x_4 = Activation('relu')(x_4)
    x_4 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block4_conv2')(x_4)
    x_4 = BatchNormalization()(x_4)
    x_4 = Activation('relu')(x_4)
    x_4 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block4_conv3')(x_4)
    x_4 = BatchNormalization()(x_4)
    x_4 = Activation('relu')(x_4)
    pool_4 = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x_4)
    # Block 5
    x_5 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block5_conv1')(pool_4)
    x_5 = BatchNormalization()(x_5)
    x_5 = Activation('relu')(x_5)
    x_5 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block5_conv2')(x_5)
    x_5 = BatchNormalization()(x_5)
    x_5 = Activation('relu')(x_5)
    x_5 = Conv2D(512, (3, 3), padding='same', kernel_initializer='he_uniform', name='block5_conv3')(x_5)
    x_5 = BatchNormalization()(x_5)
    x_5 = Activation('relu')(x_5)
    # Build model:
    model = Model(inputs=model_input, outputs=x_5)

    if weights_path:
        if weights_path == 'imagenet':
            WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.1' \
                                  '/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5 '
            weights_path = get_file('vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                    WEIGHTS_PATH_NO_TOP,
                                    cache_subdir='models')
        if os.path.exists(weights_path):
            model.load_weights(weights_path, by_name=True)

    for layer_ in model.layers[1:]:
        layer_.name += "_" + postfix

    return model, [x_4, x_3, x_2, x_1]


def cd_unet(n_classes, n_channels, weights_path='imagenet'):
    """
    Change detection unet architecture with two concatenated inputs of shape [img_wight, img_height, n_channels]

    :param n_channels: int: number of channels in the input images * 2
    :param n_classes: int: number of masks provied for prediction
    :return:
    """
    model_input = Input(shape=(None, None, n_channels), name='input')
    # get two different encoders for each of two multi-channel image
    left_encoder, left_activations = _unet_vgg16_encoder(model_input=model_input, postfix='left', weights_path=weights_path)
    right_encoder, right_activations = _unet_vgg16_encoder(model_input=model_input, postfix='right', weights_path=weights_path)
    # concatenate encoder outputs as different channels
    encoders_output = concatenate([left_encoder.output, right_encoder.output], axis=-1)
    # zip activations from encoder layers into pairs
    activations = zip(left_activations, right_activations)
    # build final model adding decoder
    model = _unet_vgg16_decoder(n_classes, model_input, encoders_output, activations)
    return model


def _conv_bn_block(_x, filters, kernel_size, name='block1_conv1', padding='same', activation='relu'):
    """
    Convolution2D + BatchNorm + ReLu block:
    """
    _x = Conv2D(filters, kernel_size, padding=padding, kernel_initializer='he_uniform', name=name)(_x)
    _x = BatchNormalization()(_x)
    _x = Activation(activation)(_x)
    return _x


def _unet_vgg16_encoder(model_input, weights_path='imagenet', postfix=""):
    """
    Simplified version of vgg16 encoder with usage of block functions:
    """
    # Crop appropriate channels (for change detection all 6 channels are doubled RGB pictures):
    input_shape = K.get_variable_shape(model_input)
    if postfix == 'left':
        ip = CroppingLayer(0, input_shape[-1] // 2)(model_input)
    elif postfix == 'right':
        ip = CroppingLayer(input_shape[-1] // 2, input_shape[-1])(model_input)
    else:
        ip = model_input
    # block 1
    x_1 = _conv_bn_block(ip, 64, (3, 3), name='block1_conv1' + '_modified' * (postfix not in {'left', 'right'}))
    x_1 = _conv_bn_block(x_1, 64, (3, 3), name='block1_conv2')
    pool_1 = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x_1)
    # block 2
    x_2 = _conv_bn_block(pool_1, 128, (3, 3), name='block2_conv1')
    x_2 = _conv_bn_block(x_2, 128, (3, 3), name='block2_conv2')
    pool_2 = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x_2)
    # block 3
    x_3 = _conv_bn_block(pool_2, 256, (3, 3), name='block3_conv1')
    x_3 = _conv_bn_block(x_3, 256, (3, 3), name='block3_conv2')
    x_3 = _conv_bn_block(x_3, 256, (3, 3), name='block3_conv3')
    pool_3 = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x_3)
    # block 4
    x_4 = _conv_bn_block(pool_3, 512, (3, 3), name='block4_conv1')
    x_4 = _conv_bn_block(x_4, 512, (3, 3), name='block4_conv2')
    x_4 = _conv_bn_block(x_4, 512, (3, 3), name='block4_conv3')
    pool_4 = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x_4)
    # block 5
    x_5 = _conv_bn_block(pool_4, 512, (3, 3), name='block5_conv1')
    x_5 = _conv_bn_block(x_5, 512, (3, 3), name='block5_conv2')
    x_5 = _conv_bn_block(x_5, 512, (3, 3), name='block5_conv3')
    # Build model:
    model = Model(inputs=model_input, outputs=x_5)
    # load weights from VGG16, pre-trained on imagenet
    if weights_path:
        if weights_path == 'imagenet':
            WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.1' \
                                  '/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5 '
            weights_path = get_file('vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                    WEIGHTS_PATH_NO_TOP,
                                    cache_subdir='models')
        if os.path.exists(weights_path):
            model.load_weights(weights_path, by_name=True)
            print('Loaded weights for {}'.format(postfix))
    # rename all layers to exclude double-named layers from the graph
    for layer_ in model.layers[1:]:
        layer_.name += "_" + postfix

    return model, [x_4, x_3, x_2, x_1]


def _unet_vgg16_decoder(n_classes, model_input, encoders_output, activations):
    """
    Simplified unet decoder for zipped activations from double-flow encoder
    """
    # Block 6
    x_4_left, x_4_right = next(activations)
    up_6 = concatenate([UpSampling2D(size=(2, 2))(encoders_output), x_4_left, x_4_right], axis=3)
    x_6 = _conv_bn_block(up_6, filters=512, kernel_size=(3, 3), name='block6_conv1')
    x_6 = _conv_bn_block(x_6, filters=512, kernel_size=(3, 3), name='block6_conv2')
    x_6 = _conv_bn_block(x_6, filters=512, kernel_size=(3, 3), name='block6_conv3')
    # Block 7
    x_3_left, x_3_right = next(activations)
    up_7 = concatenate([UpSampling2D(size=(2, 2))(x_6), x_3_left, x_3_right], axis=3)
    x_7 = _conv_bn_block(up_7, filters=256, kernel_size=(3, 3), name='block7_conv1')
    x_7 = _conv_bn_block(x_7, filters=256, kernel_size=(3, 3), name='block7_conv2')
    x_7 = _conv_bn_block(x_7, filters=256, kernel_size=(3, 3), name='block7_conv3')
    # Block 8
    x_2_left, x_2_right = next(activations)
    up_8 = concatenate([UpSampling2D(size=(2, 2))(x_7), x_2_left, x_2_right], axis=3)
    x_8 = _conv_bn_block(up_8, filters=128, kernel_size=(3, 3), name='block8_conv1')
    x_8 = _conv_bn_block(x_8, filters=128, kernel_size=(3, 3), name='block8_conv2')
    # Block 9
    x_1_left, x_1_right = next(activations)
    up_9 = concatenate([UpSampling2D(size=(2, 2))(x_8), x_1_left, x_1_right], axis=3)
    x_9 = _conv_bn_block(up_9, filters=128, kernel_size=(3, 3), name='block9_conv1')
    x_9 = _conv_bn_block(x_9, filters=128, kernel_size=(3, 3), name='block9_conv2')
    # Output
    x_10 = Conv2D(n_classes, (1, 1), activation='sigmoid', name='output')(x_9)

    model = Model(inputs=model_input, outputs=x_10)
    return model


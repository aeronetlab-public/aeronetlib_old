import keras
from keras.layers import Conv2D
from keras.layers import BatchNormalization
from keras.layers import Input
from keras.layers import Conv2DTranspose
from keras.layers import Cropping2D
from keras.layers import Activation
from keras.layers import Concatenate
from keras.applications import DenseNet169, DenseNet121, DenseNet201
from keras.models import Model

def TransposeBlock_v1(conv_channels, transp_channels, step=1):
    def layer(x):
        
        x = Conv2D(filters=int(conv_channels), kernel_size=(1, 1), 
                   name='dec_block_{}_1conv'.format(step))(x)
        x = BatchNormalization(name='dec_block_{}_1batchnorm'.format(step))(x)
        x = Activation('relu', name='dec_block_{}_1relu'.format(step))(x)

        x = Conv2DTranspose(filters=int(transp_channels), kernel_size=(4,4), strides=(2,2), padding='same',
                            kernel_initializer='he_normal', name='dec_block_{}_2transpose'.format(step))(x)
        x = BatchNormalization(name='dec_block_{}_2batchnorm'.format(step))(x)
        x = Activation('relu', name='dec_block_{}_2relu'.format(step))(x)

        x = Conv2D(filters=int(conv_channels), kernel_size=(1, 1),
                    name='dec_block_{}_3conv'.format(step))(x)
        x = BatchNormalization(name='dec_block_{}_3batchnorm'.format(step))(x)
        x = Activation('relu', name='dec_block_{}_3relu'.format(step))(x)
        return x
    return layer

def TransposeBlock_v2(conv_channels, transp_channels, step=1):
    def layer(x):
        x = Conv2D(conv_channels, 3, activation='relu', padding='same', 
                   name='dec_block_{}_conv/relu'.format(step))(x)
        x = Conv2DTranspose(transp_channels, 4, strides=(2, 2), padding='same',
                   name='dec_block_{}_transpose'.format(step))(x)
        x = Activation('relu', name='dec_block_{}_activation'.format(step))(x)
        return x
    return layer

def SegDenseNet169(input_shape=(None, None, 3), classes=1, weights='imagenet', 
              activation='softmax', n_filters=8, lock_encoder=False, deconv_block='v1'):

    encoder = DenseNet169(include_top=False, weights=weights, input_shape=input_shape)
    if lock_encoder and weights:
        for l in encoder.layers:
            l.trainable=False
    
    # take intermediate layers of encoder for skip conncetions in decoder
    skip_connections = list(reversed([4, 51, 139, 367]))
    outputs = [encoder.output]

    for i in skip_connections:
        output = encoder.layers[i].output
        outputs.append(output)

    # decoder
    
    
    if deconv_block == 'v1':
        TransposeBlock = TransposeBlock_v1
    elif deconv_block == 'v2':
        TransposeBlock = TransposeBlock_v2  
        
    x = outputs[0]

    x = TransposeBlock(n_filters*16, n_filters*8, step=1)(x)
    x = Concatenate(name='dec_concat_1')([x, outputs[1]])

    x = TransposeBlock(n_filters*8, n_filters*8, step=2)(x)
    x = Concatenate(name='dec_concat_2')([x, outputs[2]])

    x = TransposeBlock(n_filters*8, n_filters*4, step=3)(x)
    x = Concatenate(name='dec_concat_3')([x, outputs[3]])

    x = TransposeBlock(n_filters*4, n_filters*4, step=4)(x)
    x = Concatenate(name='dec_concat_4')([x, outputs[4]])

    x = TransposeBlock(n_filters*4, n_filters*2, step=5)(x)
    x = Conv2D(n_filters, 3, activation='relu', padding='same', name='dec_conv_1')(x)
    x = BatchNormalization(name='dec_batchnorm_1')(x)
    x = Activation('relu', name='dec_activation_1')(x)


    # final layer
    x = Conv2D(classes, 3, padding='same', name='dec_conv_2')(x)
    if classes > 1:
        output = Activation(activation, name='final_activation')(x)
    else:
        output = Activation('sigmoid', name='final_activation')(x)

    model = Model([encoder.input], [output]) 
    return model


def SegDenseNet121(input_shape=(None, None, 3), classes=1, weights='imagenet', 
              activation='softmax', n_filters=8, lock_encoder=False, deconv_block='v1'):

    encoder = DenseNet121(include_top=False, weights=weights, input_shape=input_shape)
    if lock_encoder and weights:
        for l in encoder.layers:
            l.trainable=False
    
    # take intermediate layers of encoder for skip conncetions in decoder
    skip_connections = list(reversed([4, 51, 139, 311]))#4, 50, 139, 367]))
    outputs = [encoder.output]

    for i in skip_connections:
        output = encoder.layers[i].output
        outputs.append(output)

    # decoder
    
    
    if deconv_block == 'v1':
        TransposeBlock = TransposeBlock_v1
    elif deconv_block == 'v2':
        TransposeBlock = TransposeBlock_v2  
        
    x = outputs[0]

    x = TransposeBlock(n_filters*16, n_filters*8, step=1)(x)
    x = Concatenate(name='dec_concat_1')([x, outputs[1]])

    x = TransposeBlock(n_filters*8, n_filters*8, step=2)(x)
    x = Concatenate(name='dec_concat_2')([x, outputs[2]])

    x = TransposeBlock(n_filters*8, n_filters*4, step=3)(x)
    x = Concatenate(name='dec_concat_3')([x, outputs[3]])

    x = TransposeBlock(n_filters*4, n_filters*4, step=4)(x)
    x = Concatenate(name='dec_concat_4')([x, outputs[4]])

    x = TransposeBlock(n_filters*4, n_filters*2, step=5)(x)
    x = Conv2D(n_filters, 3, activation='relu', padding='same', name='dec_conv_1')(x)
    x = BatchNormalization(name='dec_batchnorm_1')(x)
    x = Activation('relu', name='dec_activation_1')(x)


    # final layer
    x = Conv2D(classes, 3, padding='same', name='dec_conv_2')(x)
    if classes > 1:
        output = Activation(activation, name='final_activation')(x)
    else:
        output = Activation('sigmoid', name='final_activation')(x)

    model = Model([encoder.input], [output]) 
    return model


def SegDenseNet201(input_shape=(None, None, 3), classes=1, weights='imagenet', 
              activation='softmax', n_filters=8, lock_encoder=False, deconv_block='v1'):

    encoder = DenseNet201(include_top=False, weights=weights, input_shape=input_shape)
    if lock_encoder and weights:
        for l in encoder.layers:
            l.trainable=False
    
    # take intermediate layers of encoder for skip conncetions in decoder
    skip_connections = list(reversed([4, 51, 139, 479]))
    outputs = [encoder.output]

    for i in skip_connections:
        output = encoder.layers[i].output
        outputs.append(output)

    # decoder
    
    
    if deconv_block == 'v1':
        TransposeBlock = TransposeBlock_v1
    elif deconv_block == 'v2':
        TransposeBlock = TransposeBlock_v2  
        
    x = outputs[0]

    x = TransposeBlock(n_filters*16, n_filters*8, step=1)(x)
    x = Concatenate(name='dec_concat_1')([x, outputs[1]])

    x = TransposeBlock(n_filters*8, n_filters*8, step=2)(x)
    x = Concatenate(name='dec_concat_2')([x, outputs[2]])

    x = TransposeBlock(n_filters*8, n_filters*4, step=3)(x)
    x = Concatenate(name='dec_concat_3')([x, outputs[3]])

    x = TransposeBlock(n_filters*4, n_filters*4, step=4)(x)
    x = Concatenate(name='dec_concat_4')([x, outputs[4]])

    x = TransposeBlock(n_filters*4, n_filters*2, step=5)(x)
    x = Conv2D(n_filters, 3, activation='relu', padding='same', name='dec_conv_1')(x)
    x = BatchNormalization(name='dec_batchnorm_1')(x)
    x = Activation('relu', name='dec_activation_1')(x)


    # final layer
    x = Conv2D(classes, 3, padding='same', name='dec_conv_2')(x)
    if classes > 1:
        output = Activation(activation, name='final_activation')(x)
    else:
        output = Activation('sigmoid', name='final_activation')(x)

    model = Model([encoder.input], [output]) 
    return model



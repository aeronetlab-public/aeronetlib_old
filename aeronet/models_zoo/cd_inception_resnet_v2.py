from keras.models import Model
from keras.layers import Input
from keras.layers import Conv2D
from keras.layers import Activation
from keras.layers import concatenate
from keras.layers import Concatenate
from keras.layers import BatchNormalization

from .inception_resnet_v2 import InceptionResNetV2, TransposeBlock_v1, TransposeBlock_v2

from .layers import CroppingLayer


def cd_inception_resnet_v2(n_classes=1, input_shape=(None, None, 6), n_filters=16, activation='sigmoid'):
    """Change detection model based on Inception ResNet with block version 2:

    Args:
        input_shape: list-like: shapes of the input image (width, height, channels)
    """
    model_input = Input(shape=input_shape, name='model_input_full')

    img_pred = CroppingLayer(0, input_shape[-1] // 2)(model_input)
    img_post = CroppingLayer(input_shape[-1] // 2, input_shape[-1])(model_input)

    encoder = cd_inception_resnet_v2_encoder(n_filters=n_filters)

    #     model_input_pred = Input(shape=input_shape, name='model_input_pred')
    #     model_input_post = Input(shape=input_shape, name='model_input_post')

    encoder_outputs_pred = encoder(img_pred)
    encoder_outputs_post = encoder(img_post)

    print(encoder_outputs_pred)

    encoder_outputs = [Concatenate(name="concat_encoder_" + str(i), axis=-1)([x_pred, x_post])
                       for i, (x_pred, x_post) in enumerate(zip(encoder_outputs_pred,
                                                                encoder_outputs_post))]

    decoder = cd_inception_resnet_v2_decoder(n_classes=n_classes,
                                             n_filters=n_filters,
                                             activation=activation)

    output = decoder(encoder_outputs)

    model = Model([model_input], output)

    return model


def cd_inception_resnet_v2_encoder(input_shape=(None, None, 3), n_classes=1,
                                   weights_path=None,  # 'imagenet',
                                   activation='softmax', n_filters=16,
                                   lock_encoder=True, lock_decoder=True,
                                   deconv_block='v2'):

    encoder = InceptionResNetV2(include_top=False, weights=None)  # , input_tensor=model_input)
    if lock_encoder:  # and weights:
        for l in encoder.layers:
            l.trainable = False

    # take intermediate layers of encoder for skip conncetions in decoder
    skip_connections = list(reversed([9, 16, 260, 594]))
    outputs = [encoder.output]

    for i in skip_connections:
        output = encoder.layers[i].output
        outputs.append(output)

    # decoder

    if deconv_block == 'v1':
        TransposeBlock = TransposeBlock_v1
    elif deconv_block == 'v2':
        TransposeBlock = TransposeBlock_v2

    x = outputs[0]
    encoder_outputs = [x]

    x = TransposeBlock(n_filters * 16, n_filters * 8, step=1)(x)
    x = Concatenate(name='dec_concat_1')([x, outputs[1]])
    encoder_outputs.append(x)

    x = TransposeBlock(n_filters * 8, n_filters * 8, step=2)(x)
    x = Concatenate(name='dec_concat_2')([x, outputs[2]])
    encoder_outputs.append(x)

    x = TransposeBlock(n_filters * 8, n_filters * 4, step=3)(x)
    x = Concatenate(name='dec_concat_3')([x, outputs[3]])
    encoder_outputs.append(x)

    x = TransposeBlock(n_filters * 4, n_filters * 4, step=4)(x)
    x = Concatenate(name='dec_concat_4')([x, outputs[4]])
    encoder_outputs.append(x)

    x = TransposeBlock(n_filters * 4, n_filters * 2, step=5)(x)
    x = Conv2D(n_filters, 3, activation='relu', padding='same', name='dec_conv_1')(x)
    encoder_outputs.append(x)

    if deconv_block == 'v1':
        x = BatchNormalization(name='dec_batchnorm_1')(x)

    x = Activation('relu', name='dec_activation_1')(x)

    # final layer
    x = Conv2D(n_classes, 3, padding='same', name='dec_conv_2')(x)

    if n_classes > 1:
        output = Activation(activation, name='final_activation')(x)
    else:
        output = Activation('sigmoid', name='final_activation')(x)

    encoder_outputs.append(output)

    model = Model([encoder.input], encoder_outputs)

    if not weights_path:
        weights_path = "/home/novikov/data/change_detection/inception_resnetv2_inria_3/weights/weights.072-0.739.hdf5"
    model.load_weights(weights_path)

    return model


def cd_inception_resnet_v2_decoder(n_classes, n_filters=16, deconv_block='v2',
                                   activation='softmax'):
    inputs = [
        Input((None, None, 1536 * 2)),
        Input((None, None, 1216 * 2)),
        Input((None, None, 448 * 2)),
        Input((None, None, 256 * 2)),
        Input((None, None, 128 * 2)),
        Input((None, None, 16 * 2)),
        Input((None, None, 1 * 2))
    ]


    if deconv_block == 'v1':
        TransposeBlock = TransposeBlock_v1
    elif deconv_block == 'v2':
        TransposeBlock = TransposeBlock_v2

    x = TransposeBlock(n_filters * 16, n_filters * 8, step=6)(inputs[0])
    x = Concatenate(name='cd_dec_concat_1')([x, inputs[1]])

    x = TransposeBlock(n_filters * 8, n_filters * 8, step=7)(x)
    x = Concatenate(name='cd_dec_concat_2')([x, inputs[2]])

    x = TransposeBlock(n_filters * 8, n_filters * 4, step=8)(x)
    x = Concatenate(name='cd_dec_concat_3')([x, inputs[3]])

    x = TransposeBlock(n_filters * 4, n_filters * 4, step=9)(x)
    x = Concatenate(name='cd_dec_concat_4')([x, inputs[4]])

    x = TransposeBlock(n_filters * 4, n_filters * 2, step=10)(x)
    x = Conv2D(n_filters, 3, activation='relu', padding='same', name='cd_dec_conv_1')(x)

    #     if deconv_block == 'v1':
    #         x = BatchNormalization(name='cd_dec_batchnorm_1')(x)
    x = Concatenate(name='cd_dec_concat_5')([x, inputs[5]])
    x = Activation('relu', name='cd_dec_activation_1')(x)
    x = Conv2D(n_filters, 3, activation='relu', padding='same', name='cd_dec_conv_2')(x)

    # x = Concatenate(name='cd_dec_concat_6')([x, inputs[6]])
    # x = Activation('relu', name='cd_dec_activation_2')(x)
    # x = Conv2D(n_filters, 3, activation='relu', padding='same', name='cd_dec_conv_3')(x)

    # final layer
    x = Conv2D(n_classes, 3, padding='same', name='cd_dec_conv_4')(x)
    if n_classes > 1:
        output = Activation(activation, name='cd_final_activation')(x)
    else:
        output = Activation('sigmoid', name='cd_final_activation')(x)

    model = Model(inputs, [output])

    return model
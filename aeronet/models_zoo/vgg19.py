from keras.models import Model
from keras.layers import Input
from keras.layers import Conv2D
from keras.layers import Cropping2D
from keras.layers import Activation
from keras.layers import concatenate
from keras.layers import MaxPooling2D
from keras.layers import UpSampling2D
from keras.utils.data_utils import get_file
from keras.layers.advanced_activations import ELU
from keras.layers.normalization import BatchNormalization


def get_VGG19(input_shape):
    WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg19_weights_tf_dim_ordering_tf_kernels_notop.h5'

    input = Input(shape=input_shape)
    # Block 1
    x1 = Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv1')(input)
    x1 = Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv2')(x1)
    x1 = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x1)

    # Block 2
    x2 = Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv1')(x1)
    x2 = Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv2')(x2)
    x2 = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x2)

    # Block 3
    x3 = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv1')(x2)
    x3 = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv2')(x3)
    x3 = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv3')(x3)
    x3 = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv4')(x3)
    x3 = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x3)

    # Block 4
    x4 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv1')(x3)
    x4 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv2')(x4)
    x4 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv3')(x4)
    x4 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv4')(x4)
    x4 = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x4)

    # Block 5
    x5 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv1')(x4)
    x5 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv2')(x5)
    x5 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv3')(x5)
    x5 = Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv4')(x5)
    x5 = MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool')(x5)
    model = Model(input, [x1, x2, x3, x4, x5] , name='vgg19')
    
    weights_path = get_file('vgg19_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                    WEIGHTS_PATH_NO_TOP,
                                    cache_subdir='models',
                                    file_hash='253f8cb515780f3b799900260a226db6')
    model.load_weights(weights_path)
    return model
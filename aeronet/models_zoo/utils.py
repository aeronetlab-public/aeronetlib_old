from keras.layers import Lambda


def CroppingLayer(start, end):
    """
    Crops (or slice) a Tensor from start to end

    Args:
        start: int: starting element of the slice
        end: int: ending element, excluded
    """

    def _crop(x):
        return x[:, :, :, start: end]

    return Lambda(_crop)
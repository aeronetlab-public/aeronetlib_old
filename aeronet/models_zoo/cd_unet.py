import os

import keras.backend as K

from keras.models import Model
from keras.layers import Input
from keras.layers import Conv2D
from keras.layers import Activation
from keras.layers import concatenate
from keras.layers import MaxPooling2D
from keras.layers import UpSampling2D
from keras.utils.data_utils import get_file
from keras.layers.normalization import BatchNormalization

from .layers import CroppingLayer


def cd_unet(n_classes, n_channels, weights_path='imagenet'):
    """
    Change detection unet architecture with two concatenated inputs of shape [img_wight, img_height, n_channels]

    :param n_channels: int: number of channels in the input images * 2
    :param n_classes: int: number of masks provied for prediction
    :param weights_path: str:
    :return:
    """
    model_input = Input(shape=(None, None, n_channels), name='input')
    # get two different encoders for each of two multi-channel image
    left_encoder, left_activations = _unet_vgg16_encoder(model_input=model_input, postfix='left', weights_path=weights_path)
    right_encoder, right_activations = _unet_vgg16_encoder(model_input=model_input, postfix='right', weights_path=weights_path)
    # concatenate encoder outputs as different channels
    encoders_output = concatenate([left_encoder.output, right_encoder.output], axis=-1)
    # zip activations from encoder layers into pairs
    activations = zip(left_activations, right_activations)
    # build final model adding decoder
    model = _unet_vgg16_decoder(n_classes, model_input, encoders_output, activations)
    return model


def _conv_bn_block(_x, filters, kernel_size, name='block1_conv1', padding='same', activation='relu'):
    """
    Convolution2D + BatchNorm + ReLu block:
    """
    _x = Conv2D(filters, kernel_size, padding=padding, kernel_initializer='he_uniform', name=name)(_x)
    _x = BatchNormalization()(_x)
    _x = Activation(activation)(_x)
    return _x


def _unet_vgg16_encoder(model_input, weights_path='imagenet', postfix=""):
    """
    Simplified version of vgg16 encoder with usage of block functions:
    """
    # Crop appropriate channels (for change detection all 6 channels are doubled RGB pictures):
    input_shape = K.get_variable_shape(model_input)
    if postfix == 'left':
        ip = CroppingLayer(0, input_shape[-1] // 2)(model_input)
    elif postfix == 'right':
        ip = CroppingLayer(input_shape[-1] // 2, input_shape[-1])(model_input)
    else:
        ip = model_input
    # block 1
    x_1 = _conv_bn_block(ip, 64, (3, 3), name='block1_conv1' + '_modified' * (postfix not in {'left', 'right'}))
    x_1 = _conv_bn_block(x_1, 64, (3, 3), name='block1_conv2')
    pool_1 = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x_1)
    # block 2
    x_2 = _conv_bn_block(pool_1, 128, (3, 3), name='block2_conv1')
    x_2 = _conv_bn_block(x_2, 128, (3, 3), name='block2_conv2')
    pool_2 = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x_2)
    # block 3
    x_3 = _conv_bn_block(pool_2, 256, (3, 3), name='block3_conv1')
    x_3 = _conv_bn_block(x_3, 256, (3, 3), name='block3_conv2')
    x_3 = _conv_bn_block(x_3, 256, (3, 3), name='block3_conv3')
    pool_3 = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x_3)
    # block 4
    x_4 = _conv_bn_block(pool_3, 512, (3, 3), name='block4_conv1')
    x_4 = _conv_bn_block(x_4, 512, (3, 3), name='block4_conv2')
    x_4 = _conv_bn_block(x_4, 512, (3, 3), name='block4_conv3')
    pool_4 = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x_4)
    # block 5
    x_5 = _conv_bn_block(pool_4, 512, (3, 3), name='block5_conv1')
    x_5 = _conv_bn_block(x_5, 512, (3, 3), name='block5_conv2')
    x_5 = _conv_bn_block(x_5, 512, (3, 3), name='block5_conv3')
    # Build model:
    model = Model(inputs=model_input, outputs=x_5)
    # load weights from VGG16, pre-trained on imagenet
    if weights_path:
        if weights_path == 'imagenet':
            WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.1' \
                                  '/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5 '
            weights_path = get_file('vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                    WEIGHTS_PATH_NO_TOP,
                                    cache_subdir='models')
        if os.path.exists(weights_path):
            model.load_weights(weights_path, by_name=True)
    # rename all layers to exclude double-named layers from the graph
    for layer_ in model.layers[1:]:
        layer_.name += "_" + postfix

    return model, [x_4, x_3, x_2, x_1]


def _unet_vgg16_decoder(n_classes, model_input, encoders_output, activations):
    """
    Simplified unet decoder for zipped activations from double-flow encoder
    """
    # Block 6
    x_4_left, x_4_right = next(activations)
    up_6 = concatenate([UpSampling2D(size=(2, 2))(encoders_output), x_4_left, x_4_right], axis=3)
    x_6 = _conv_bn_block(up_6, filters=512, kernel_size=(3, 3), name='block6_conv1')
    x_6 = _conv_bn_block(x_6, filters=512, kernel_size=(3, 3), name='block6_conv2')
    x_6 = _conv_bn_block(x_6, filters=512, kernel_size=(3, 3), name='block6_conv3')
    # Block 7
    x_3_left, x_3_right = next(activations)
    up_7 = concatenate([UpSampling2D(size=(2, 2))(x_6), x_3_left, x_3_right], axis=3)
    x_7 = _conv_bn_block(up_7, filters=256, kernel_size=(3, 3), name='block7_conv1')
    x_7 = _conv_bn_block(x_7, filters=256, kernel_size=(3, 3), name='block7_conv2')
    x_7 = _conv_bn_block(x_7, filters=256, kernel_size=(3, 3), name='block7_conv3')
    # Block 8
    x_2_left, x_2_right = next(activations)
    up_8 = concatenate([UpSampling2D(size=(2, 2))(x_7), x_2_left, x_2_right], axis=3)
    x_8 = _conv_bn_block(up_8, filters=128, kernel_size=(3, 3), name='block8_conv1')
    x_8 = _conv_bn_block(x_8, filters=128, kernel_size=(3, 3), name='block8_conv2')
    # Block 9
    x_1_left, x_1_right = next(activations)
    up_9 = concatenate([UpSampling2D(size=(2, 2))(x_8), x_1_left, x_1_right], axis=3)
    x_9 = _conv_bn_block(up_9, filters=128, kernel_size=(3, 3), name='block9_conv1')
    x_9 = _conv_bn_block(x_9, filters=128, kernel_size=(3, 3), name='block9_conv2')
    # Output
    x_10 = Conv2D(n_classes, (1, 1), activation='sigmoid', name='output')(x_9)

    model = Model(inputs=model_input, outputs=x_10)
    return model


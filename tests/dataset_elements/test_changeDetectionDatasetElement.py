from unittest import TestCase

from aeronet.datasets.segmentation.segmentation_dataset_element import SegmentationDatasetElement
from aeronet.datasets.change_detection.cd_dataset_element import ChangeDetectionDatasetElement
from .image_testing import assert_orthophoto
from tests import root


class TestChangeDetectionDatasetElement(TestCase):
    def test_element(self):
        timeframes = ['20170304', '20170404']
        gsd = [0.6, -0.6]
        shape = (1099, 1524, 6)
        crs = {'init': 'EPSG:3857'}

        segmentation_elements = dict((timeframe, SegmentationDatasetElement(
            [f"{root}/resources/ventura/ventura_000009/{timeframe}/ventura_000009_channel_{channel}.tif"
             for channel in ['RED', 'GRN', 'BLU']], target_gsd=gsd)) for timeframe in timeframes)
        cd_element = ChangeDetectionDatasetElement(segmentation_elements)

        elements = cd_element.get_elements(descending=True)
        assert len(elements) == 2
        assert elements[0] is segmentation_elements[timeframes[1]]

        elements = cd_element.get_elements(descending=False)
        assert elements[0] is segmentation_elements[timeframes[0]]

        assert_orthophoto(cd_element, shape, gsd, crs)

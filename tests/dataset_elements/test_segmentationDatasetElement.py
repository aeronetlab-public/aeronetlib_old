from unittest import TestCase
from .image_testing import test_image
from .image_testing import assert_orthophoto
from aeronet.datasets.segmentation.segmentation_dataset_element import SegmentationDatasetElement
from tests import root
from rasterio.crs import CRS


class TestSegmentationDatasetElement(TestCase):

    def test_element(self):
        # initial parameters
        gsd = [0.6, -0.6]
        crs = {'init': 'epsg:3857'}

        sources3857 = [f"{root}/resources/ventura/3857/ventura_000009_channel_{channel}.tif" for channel in
                       ['RED', 'GRN', 'BLU']]
        shape = (1099, 1524, 3)
        element3857 = SegmentationDatasetElement(sources3857, target_gsd=gsd)
        assert_orthophoto(element3857, shape, gsd, crs)

        sources4326 = [f"{root}/resources/ventura/4326/ventura_000009_channel_{channel}.tif" for channel in
                       ['RED', 'GRN', 'BLU']]
        element4326 = SegmentationDatasetElement(sources4326, target_gsd=gsd)
        assert_orthophoto(element4326, shape, gsd, crs)

        shape = (1099, 1524, 2)
        element3857 = SegmentationDatasetElement(sources3857[1:], target_gsd=gsd)
        assert_orthophoto(element3857, shape, gsd, crs)

        binary_sources3857 = [test_image(path) for path in sources3857]
        shape = (1099, 1524, 3)
        element3857 = SegmentationDatasetElement(binary_sources3857, target_gsd=gsd)
        assert_orthophoto(element3857, shape, gsd, crs)

        binary_sources4326 = [test_image(path) for path in sources4326]
        element4326 = SegmentationDatasetElement(binary_sources4326, target_gsd=gsd)
        assert_orthophoto(element4326, shape, gsd, crs)

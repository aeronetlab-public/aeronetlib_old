from io import BytesIO


def test_image(path):
    with open(path, 'rb') as f:
        img = BytesIO(f.read())
    return img


def assert_orthophoto(element, expected_shape, expected_gsd, expected_crs):
    channels = element.get_channels()
    crs = element.get_crs()
    transform = element.get_transform()
    assert crs == expected_crs
    assert (channels.shape == expected_shape)
    delta = 0.05
    assert (abs(expected_gsd[0] - transform[0]) < delta)
    assert (abs(expected_gsd[1] - transform[4]) < delta)

    channels = element.get_channels()
    # assert image was not read twice
    assert (channels is not None)

def crs_as_string(crs):
    if isinstance(crs, str):
        return crs
    else:
        return crs['init']
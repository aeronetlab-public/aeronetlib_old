from setuptools import setup
from setuptools import find_packages
 
setup(name='aeronet',
      version='0.1.2',
      description='Deep Learning for Remote Sensing',
      author='AeroNet Lab, Skolkovo Institute of Science and Technology',
      author_email='aeronet@skoltech.ru',
      url='no',
      download_url='no',
      license='Proprietary',
      install_requires=[],
      extras_require={
          'h5py': ['h5py'],
      },
      classifiers=[
      ],
      packages=find_packages(include=["aeronet*"]))
